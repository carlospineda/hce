jQuery(function () {

    fechaHora();


    jQuery(".fancybox").fancybox({
        helpers: {
            title: {
                type: 'inside',
                position: 'top'
            }
        },
        openEffect: 'elastic',
        closeEffect: 'elastic'
    });

    jQuery("a.iframe").fancybox({
        helpers: {
            title: {
                type: 'inside',
                position: 'top'
            }
        },
        openEffect: 'elastic',
        closeEffect: 'elastic',
        iframe : {
            preload: false
        }

    });

    // Seleccionar fila del boton de acciones
    $('.actions .dropdown-toggle').click(function () {

        if ($(this).parent().hasClass("open")) {
            $(this).parents('tr').addClass('filaseleccionada');
        } else {
            $(this).parents('tr').removeClass('filaseleccionada');
        }

    });

    $('.actions').focusout(function () {
        $('body').removeClass('filaseleccionada');
    });

    // Fin carga de la pagina
    setTimeout('jQuery("html").removeClass("loading")', 1000);


});


/**
 * Funcion para mostrar la hora actual en el panel superior deslizante de la plantilla
 */
function fechaHora() {
    var Digital = new Date();

    var hours = Digital.getHours();
    var minutes = Digital.getMinutes();

    var dia = Digital.getDate();
    var mes = Digital.getMonth() + 1;
    var anio = Digital.getFullYear();

    var dn = "PM"

    if (hours < 12)
        dn = "AM"
    if (hours > 12)
        hours = hours - 12;
    if (hours == 0)
        hours = 12
    if (minutes <= 9)
        minutes = "0" + minutes;

    if (dia < 10)
        dia = '0' + dia;
    if (mes < 10)
        mes = '0' + mes;

    jQuery('#hora-actual').html(hours + ":" + minutes + " " + dn);
    jQuery('#fecha-actual').html(dia + "/" + mes + "/" + anio);

    setTimeout("fechaHora()", 10000);
}

/**
 * Genera mensaje Growl de notificación
 * @param titulo
 * @param msg
 * @param type
 * @param icon
 */
function mensaje(titulo, msg, type = 'warning', icon = 'fa-exclamation-triangle') {

    $.growl({
        icon: 'fa ' + icon,
        title: titulo,
        message: msg
    }, {
        template: {
            title_divider: '<hr class="separator" />'
        },
        type: type
    });

}

function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}