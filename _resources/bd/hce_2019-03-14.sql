# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.31-MariaDB)
# Base de datos: hce
# Tiempo de Generación: 2019-03-15 01:04:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla ci_configuracion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_configuracion`;

CREATE TABLE `ci_configuracion` (
  `idConfiguracion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `valor` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idConfiguracion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_cookies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_cookies`;

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_rol`;

CREATE TABLE `ci_rol` (
  `idRol` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `codigo` char(1) NOT NULL DEFAULT '',
  `urlInicio` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) NOT NULL DEFAULT '0',
  `user_data` mediumtext NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_usuario`;

CREATE TABLE `ci_usuario` (
  `idUsuario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `tipoDocumento` char(2) DEFAULT NULL COMMENT 'CC : CedulaCiudadania - NI : Nit - CE : CedulaExtrangeria',
  `numDocumento` varchar(30) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `desde` datetime DEFAULT NULL,
  `ultimoAcceso` datetime DEFAULT NULL,
  `idRol` int(11) unsigned NOT NULL COMMENT 'A:Administrador - I:Ingeniero - M:Maestro',
  `urlAvatar` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Habilitado - 0:Deshabilitado',
  PRIMARY KEY (`idUsuario`),
  KEY `fk_idRol_idRol` (`idRol`),
  CONSTRAINT `fk_idRol_idRol` FOREIGN KEY (`idRol`) REFERENCES `ci_rol` (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_ciudad
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_ciudad`;

CREATE TABLE `hc_ciudad` (
  `idCiudad` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idDepartamento` int(11) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idCiudad`),
  KEY `FK_departamento` (`idDepartamento`),
  CONSTRAINT `FK_departamento` FOREIGN KEY (`idDepartamento`) REFERENCES `hc_departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_departamento
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_departamento`;

CREATE TABLE `hc_departamento` (
  `idDepartamento` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idPais` int(11) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idDepartamento`),
  KEY `FK_pais` (`idPais`),
  CONSTRAINT `FK_pais` FOREIGN KEY (`idPais`) REFERENCES `hc_pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_estadocivil
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_estadocivil`;

CREATE TABLE `hc_estadocivil` (
  `idEstadoCivil` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `estadoCivil` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idEstadoCivil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_factorrh
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_factorrh`;

CREATE TABLE `hc_factorrh` (
  `idFactorRh` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `factor` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idFactorRh`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_genero
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_genero`;

CREATE TABLE `hc_genero` (
  `idGenero` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `genero` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_pais
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_pais`;

CREATE TABLE `hc_pais` (
  `idPais` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_tercero
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_tercero`;

CREATE TABLE `hc_tercero` (
  `idTercero` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la Tabla',
  `hash` varchar(255) NOT NULL,
  `primerNombre` varchar(64) DEFAULT NULL COMMENT 'Primer Nombre',
  `segundoNombre` varchar(64) DEFAULT NULL COMMENT 'Segundo Nombre',
  `primerApellido` varchar(64) DEFAULT NULL COMMENT 'Primer Apellido',
  `segundoApellido` varchar(64) DEFAULT NULL COMMENT 'Segundo Apellido',
  `idTipoDocumento` int(11) unsigned DEFAULT NULL COMMENT 'Identificador Tipo Documento',
  `numeroDocumento` varchar(13) DEFAULT NULL COMMENT 'Numero de Documento',
  `fechaDocumento` date DEFAULT NULL COMMENT 'Fecha Documento',
  `idCiudadExpDocumento` int(11) unsigned DEFAULT NULL COMMENT 'Identificador de Division Politica',
  `fechaNacimiento` date DEFAULT NULL COMMENT 'Fecha Nacimiento',
  `idCiudadNacimiento` int(11) unsigned DEFAULT NULL COMMENT 'Identificador de Division Politica',
  `idGenero` int(11) unsigned DEFAULT NULL COMMENT 'Identificador Genero',
  `idEstadoCivil` int(11) unsigned DEFAULT NULL COMMENT 'Estado Civil',
  `idFactorRh` int(11) unsigned DEFAULT NULL COMMENT 'Identificador Factor RH',
  `fechaCreacion` date DEFAULT NULL COMMENT 'Fecha Creacion',
  PRIMARY KEY (`idTercero`),
  UNIQUE KEY `IX_Terceros` (`idTipoDocumento`,`numeroDocumento`),
  KEY `IX_Gen` (`idGenero`),
  KEY `IX_TipDoc` (`idTipoDocumento`),
  KEY `FK_ciudadexpedicion` (`idCiudadExpDocumento`),
  KEY `FK_ciudadnacimiento` (`idCiudadNacimiento`),
  KEY `FK_estadocivil` (`idEstadoCivil`),
  KEY `FK_factorrh` (`idFactorRh`),
  CONSTRAINT `FK_ciudadexpedicion` FOREIGN KEY (`idCiudadExpDocumento`) REFERENCES `hc_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ciudadnacimiento` FOREIGN KEY (`idCiudadNacimiento`) REFERENCES `hc_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_estadocivil` FOREIGN KEY (`idEstadoCivil`) REFERENCES `hc_estadocivil` (`idEstadoCivil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_factorrh` FOREIGN KEY (`idFactorRh`) REFERENCES `hc_factorrh` (`idFactorRh`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_genero` FOREIGN KEY (`idGenero`) REFERENCES `hc_genero` (`idGenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tipodocumento` FOREIGN KEY (`idTipoDocumento`) REFERENCES `hc_tipodocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_tercerocontacto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_tercerocontacto`;

CREATE TABLE `hc_tercerocontacto` (
  `idTerceroContacto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idTercero` int(11) unsigned NOT NULL,
  `idTipoContacto` int(11) unsigned NOT NULL,
  `idCiudad` int(11) unsigned DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `barrio` varchar(255) DEFAULT NULL,
  `telefonoFijo` varchar(255) DEFAULT NULL,
  `telefonoCelular` varchar(255) DEFAULT NULL,
  `correoElectronico` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTerceroContacto`),
  KEY `FK_tipocontacto` (`idTipoContacto`),
  KEY `FK_tercero` (`idTercero`),
  KEY `FK_ciudad` (`idCiudad`),
  CONSTRAINT `FK_ciudad` FOREIGN KEY (`idCiudad`) REFERENCES `hc_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tercero` FOREIGN KEY (`idTercero`) REFERENCES `hc_tercero` (`idTercero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tipocontacto` FOREIGN KEY (`idTipoContacto`) REFERENCES `hc_tipocontacto` (`idTipoContacto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_tipocontacto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_tipocontacto`;

CREATE TABLE `hc_tipocontacto` (
  `idTipoContacto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idTipoContacto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla hc_tipodocumento
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hc_tipodocumento`;

CREATE TABLE `hc_tipodocumento` (
  `idTipoDocumento` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipoDocumento` varchar(255) NOT NULL DEFAULT '',
  `codTipoDocumento` varchar(4) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
