-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-11-2018 a las 09:50:43
-- Versión del servidor: 5.6.41
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cbrdx_directrix`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_cookies`
--

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_rol`
--

CREATE TABLE `ci_rol` (
  `idRol` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `codigo` char(1) NOT NULL DEFAULT '',
  `urlInicio` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ci_rol`
--

INSERT INTO `ci_rol` (`idRol`, `nombre`, `codigo`, `urlInicio`, `indHabilitado`) VALUES
(1, 'SuperAdmin', 'S', 'superadmin/dashboard', 1),
(2, 'Admin', 'A', 'admin/dashboard', 1),
(3, 'Usuario', 'U', 'usuario/dashboard', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) NOT NULL DEFAULT '0',
  `user_data` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5b7be64a5ba75a4c2ee87d8b93e5adf0', '181.236.246.158', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.3', 1542748486, 'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";}'),
('a0deb7fac7a4bb3a5c3c0b51ae5d9f0c', '181.236.246.158', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.3', 1542748905, 'a:10:{s:9:\"user_data\";s:0:\"\";s:5:\"us_id\";s:1:\"9\";s:9:\"us_nombre\";s:5:\"Karen\";s:11:\"us_apellido\";s:12:\"Pinto Romero\";s:9:\"us_avatar\";s:58:\"http://ciberdix.com/directrix/assets/uploads/avatar/Kp.jpg\";s:8:\"us_email\";s:23:\"karenpinto@ciberdix.com\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";s:12:\"is_logged_in\";b:1;}'),
('b4e3bfddb3b9745b39c8e30826d86205', '191.102.83.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.3', 1542748870, 'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_usuario`
--

CREATE TABLE `ci_usuario` (
  `idUsuario` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `tipoDocumento` char(2) DEFAULT NULL COMMENT 'CC : CedulaCiudadania - NI : Nit - CE : CedulaExtrangeria',
  `numDocumento` varchar(30) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `desde` datetime DEFAULT NULL,
  `ultimoAcceso` datetime DEFAULT NULL,
  `idRol` int(11) UNSIGNED NOT NULL COMMENT 'A:Administrador - I:Ingeniero - M:Maestro',
  `urlAvatar` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Habilitado - 0:Deshabilitado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ci_usuario`
--

INSERT INTO `ci_usuario` (`idUsuario`, `email`, `contrasena`, `tipoDocumento`, `numDocumento`, `nombre`, `apellido`, `direccion`, `telefono`, `celular`, `desde`, `ultimoAcceso`, `idRol`, `urlAvatar`, `indHabilitado`) VALUES
(1, 'admin@ciberdix.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CC', '123', 'Admin', 'ciberdix', NULL, NULL, NULL, '0000-00-00 00:00:00', '2016-05-08 12:39:28', 2, 'ciberdix-circle.jpg', 1),
(2, 'superadmin@ciberdix.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CC', '456', 'SuperAdmin', 'ciberdix', NULL, NULL, NULL, '0000-00-00 00:00:00', '2018-11-16 12:11:23', 1, 'ciberdix-circle.jpg', 1),
(3, 'usuario@ciberdix.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CC', '789', 'Usuario', 'ciberdix', NULL, NULL, NULL, '0000-00-00 00:00:00', '2016-05-08 12:39:28', 3, '36d35-logo-02.png', 1),
(8, 'carlospineda@ciberdix.com', '81dc9bdb52d04dc20036dbd8313ed055', 'cc', '13746254', 'Carlos Alberto', 'Pineda Torres', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1),
(9, 'karenpinto@ciberdix.com', 'b325ebd3636ecab0fa1554e948636f81', 'CC', '1098773264', 'Karen', 'Pinto Romero', 'Cra 59 #146 a 41 el carmen 6 etapa', '6183499', '3168682098', '2018-11-15 14:02:12', '2018-11-20 16:21:47', 1, 'Kp.jpg', 1),
(10, 'andres.munoz@ciberdix.com', '81dc9bdb52d04dc20036dbd8313ed055', 'CC', '1098773264', 'Anrecito', 'Muñucito', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dx_documento`
--

CREATE TABLE `dx_documento` (
  `idDocumento` int(11) UNSIGNED NOT NULL,
  `idTipoDocumento` int(11) UNSIGNED NOT NULL,
  `idProceso` int(11) UNSIGNED NOT NULL,
  `codigo` varchar(255) NOT NULL DEFAULT '',
  `titulo` varchar(255) NOT NULL DEFAULT '',
  `fechaHora` timestamp NULL DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dx_documento`
--

INSERT INTO `dx_documento` (`idDocumento`, `idTipoDocumento`, `idProceso`, `codigo`, `titulo`, `fechaHora`, `indHabilitado`) VALUES
(1, 4, 1, '001', 'Politica de Calidad', '2018-01-01 06:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dx_proceso`
--

CREATE TABLE `dx_proceso` (
  `idProceso` int(11) UNSIGNED NOT NULL,
  `idTipoProceso` int(11) UNSIGNED NOT NULL,
  `codigo` varchar(255) NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dx_proceso`
--

INSERT INTO `dx_proceso` (`idProceso`, `idTipoProceso`, `codigo`, `nombre`, `indHabilitado`) VALUES
(1, 1, 'GEREN', 'Gerencia', 1),
(2, 1, 'CALID', 'Calidad', 1),
(3, 2, 'GEPRO', 'Proyectos ', 1),
(4, 2, 'DIDES', 'Diseño y Desarrollo', 1),
(5, 2, 'SOPOR', 'Soporte y Mantenimiento', 1),
(6, 2, 'MEVEN', 'Mercadeo y Ventas', 1),
(7, 3, 'ADMIN', 'Administración', 1),
(8, 3, 'TAHUM', 'Talento Humano', 1),
(9, 3, 'SGSST', 'Seguridad y Salud en el Trabajo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dx_tipodocumento`
--

CREATE TABLE `dx_tipodocumento` (
  `idTipoDocumento` int(11) UNSIGNED NOT NULL,
  `codigo` varchar(10) NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `orden` tinyint(2) NOT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dx_tipodocumento`
--

INSERT INTO `dx_tipodocumento` (`idTipoDocumento`, `codigo`, `nombre`, `orden`, `indHabilitado`) VALUES
(3, 'F', 'Formato', 2, 1),
(4, 'D', 'Documento', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dx_tipoproceso`
--

CREATE TABLE `dx_tipoproceso` (
  `idTipoProceso` int(11) UNSIGNED NOT NULL,
  `codigo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dx_tipoproceso`
--

INSERT INTO `dx_tipoproceso` (`idTipoProceso`, `codigo`, `nombre`, `indHabilitado`) VALUES
(1, 'GES', 'Proceso de Gestión', 1),
(2, 'MIS', 'Proceso Misionales', 1),
(3, 'APO', 'Proceso de Apoyo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dx_versiondocumento`
--

CREATE TABLE `dx_versiondocumento` (
  `idVersionDocumento` int(11) UNSIGNED NOT NULL,
  `idDocumento` int(11) UNSIGNED NOT NULL,
  `idUsuario` int(11) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL DEFAULT '',
  `urlOpen` varchar(255) DEFAULT NULL,
  `urlPdf` varchar(255) DEFAULT NULL,
  `fechaHora` timestamp NULL DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dx_versiondocumento`
--

INSERT INTO `dx_versiondocumento` (`idVersionDocumento`, `idDocumento`, `idUsuario`, `version`, `urlOpen`, `urlPdf`, `fechaHora`, `indHabilitado`) VALUES
(1, 1, 9, '0.1', 'a6102-d-geren-001-politica-de-calidad-v.01.docx', '159df-d-geren-001-politica-de-calidad-v.01.pdf', '2018-11-20 18:07:57', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ci_cookies`
--
ALTER TABLE `ci_cookies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ci_rol`
--
ALTER TABLE `ci_rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indices de la tabla `ci_usuario`
--
ALTER TABLE `ci_usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `fk_idRol_idRol` (`idRol`);

--
-- Indices de la tabla `dx_documento`
--
ALTER TABLE `dx_documento`
  ADD PRIMARY KEY (`idDocumento`),
  ADD KEY `dx_documento_ibfk_2` (`idProceso`),
  ADD KEY `dx_documento_ibfk_1` (`idTipoDocumento`);

--
-- Indices de la tabla `dx_proceso`
--
ALTER TABLE `dx_proceso`
  ADD PRIMARY KEY (`idProceso`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `idProceso` (`idTipoProceso`);

--
-- Indices de la tabla `dx_tipodocumento`
--
ALTER TABLE `dx_tipodocumento`
  ADD PRIMARY KEY (`idTipoDocumento`);

--
-- Indices de la tabla `dx_tipoproceso`
--
ALTER TABLE `dx_tipoproceso`
  ADD PRIMARY KEY (`idTipoProceso`);

--
-- Indices de la tabla `dx_versiondocumento`
--
ALTER TABLE `dx_versiondocumento`
  ADD PRIMARY KEY (`idVersionDocumento`),
  ADD KEY `idDocumento` (`idDocumento`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ci_cookies`
--
ALTER TABLE `ci_cookies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ci_rol`
--
ALTER TABLE `ci_rol`
  MODIFY `idRol` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ci_usuario`
--
ALTER TABLE `ci_usuario`
  MODIFY `idUsuario` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `dx_documento`
--
ALTER TABLE `dx_documento`
  MODIFY `idDocumento` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `dx_proceso`
--
ALTER TABLE `dx_proceso`
  MODIFY `idProceso` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `dx_tipodocumento`
--
ALTER TABLE `dx_tipodocumento`
  MODIFY `idTipoDocumento` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `dx_tipoproceso`
--
ALTER TABLE `dx_tipoproceso`
  MODIFY `idTipoProceso` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dx_versiondocumento`
--
ALTER TABLE `dx_versiondocumento`
  MODIFY `idVersionDocumento` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ci_usuario`
--
ALTER TABLE `ci_usuario`
  ADD CONSTRAINT `fk_idRol_idRol` FOREIGN KEY (`idRol`) REFERENCES `ci_rol` (`idRol`);

--
-- Filtros para la tabla `dx_documento`
--
ALTER TABLE `dx_documento`
  ADD CONSTRAINT `dx_documento_ibfk_1` FOREIGN KEY (`idTipoDocumento`) REFERENCES `dx_tipodocumento` (`idTipoDocumento`),
  ADD CONSTRAINT `dx_documento_ibfk_2` FOREIGN KEY (`idProceso`) REFERENCES `dx_proceso` (`idProceso`);

--
-- Filtros para la tabla `dx_proceso`
--
ALTER TABLE `dx_proceso`
  ADD CONSTRAINT `dx_proceso_ibfk_1` FOREIGN KEY (`idTipoProceso`) REFERENCES `dx_tipoproceso` (`idTipoProceso`);

--
-- Filtros para la tabla `dx_versiondocumento`
--
ALTER TABLE `dx_versiondocumento`
  ADD CONSTRAINT `dx_versiondocumento_ibfk_1` FOREIGN KEY (`idDocumento`) REFERENCES `dx_documento` (`idDocumento`),
  ADD CONSTRAINT `dx_versiondocumento_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `ci_usuario` (`idUsuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
