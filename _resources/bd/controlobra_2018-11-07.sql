# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.31-MariaDB)
# Base de datos: controlobra
# Tiempo de Generación: 2018-11-07 16:03:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla ci_cookies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_cookies`;

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) NOT NULL DEFAULT '0',
  `user_data` mediumtext NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('1ed34c94efa20d0d0105f7e6b68ff603','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.3',1525116440,'a:1:{s:9:\"user_data\";s:0:\"\";}'),
	('40f6fd8ae8003940f1084c47501438e9','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.3',1525119559,'a:8:{s:9:\"user_data\";s:0:\"\";s:5:\"us_id\";N;s:9:\"us_nombre\";s:13:\"Administrador\";s:11:\"us_apellido\";s:11:\"ControlObra\";s:6:\"us_rol\";s:1:\"A\";s:9:\"us_avatar\";s:155:\"http://www.gravatar.com/avatar/79698a701fdc9b4f1893e4edbe7a600f?d=http%3A%2F%2Flocalhost%2Fcontrolobra%2Fweb%2Fassets%2Fuploads%2Favatar%2Favatar.png&s=100\";s:8:\"us_email\";s:16:\"admin@inacar.com\";s:12:\"is_logged_in\";b:1;}'),
	('b80be084420d91a0a1c68f56d73ecd24','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.3',1525116476,'a:1:{s:9:\"user_data\";s:0:\"\";}'),
	('dc693d12c04343c17f518e176d832c2c','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.3',1525116440,'a:1:{s:9:\"user_data\";s:0:\"\";}'),
	('f27399b9daf417822d07db6e134b4eaf','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.3',1525116451,'a:1:{s:9:\"user_data\";s:0:\"\";}');

/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla ci_usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_usuario`;

CREATE TABLE `ci_usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `tipoDocumento` char(2) DEFAULT NULL COMMENT 'CC : CedulaCiudadania - NI : Nit - CE : CedulaExtrangeria',
  `numDocumento` varchar(30) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `desde` datetime DEFAULT NULL,
  `ultimoAcceso` datetime DEFAULT NULL,
  `empresa` varchar(255) DEFAULT NULL,
  `tipo` char(1) NOT NULL DEFAULT 'M' COMMENT 'A:Administrador - I:Ingeniero - M:Maestro',
  `urlAvatar` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Habilitado - 0:Deshabilitado',
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_usuario` WRITE;
/*!40000 ALTER TABLE `ci_usuario` DISABLE KEYS */;

INSERT INTO `ci_usuario` (`idUsuario`, `email`, `contrasena`, `tipoDocumento`, `numDocumento`, `nombre`, `apellido`, `direccion`, `telefono`, `celular`, `desde`, `ultimoAcceso`, `empresa`, `tipo`, `urlAvatar`, `indHabilitado`)
VALUES
	(1,'admin@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055',NULL,NULL,'SuperAdministrador','ControlObra',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28','Ciberdix','S','36d35-logo-02.png',1),
	(2,'admin@inacar.com','81dc9bdb52d04dc20036dbd8313ed055',NULL,NULL,'Administrador','ControlObra',NULL,NULL,NULL,NULL,'2018-01-31 15:38:47','Inacar','A',NULL,1),
	(3,'alvaromartinez@inacar.com','81dc9bdb52d04dc20036dbd8313ed055','CC','1098627312','ALVARO ','MARTINEZ','Calle 15NC 26-29','6732840','3108652285','2016-05-10 00:00:00',NULL,'INACAR S.A.','M',NULL,1);

/*!40000 ALTER TABLE `ci_usuario` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
