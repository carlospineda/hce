# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.31-MariaDB)
# Base de datos: directrix
# Tiempo de Generación: 2018-11-09 16:20:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla ci_cookies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_cookies`;

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_rol`;

CREATE TABLE `ci_rol` (
  `idRol` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `codigo` char(1) NOT NULL DEFAULT '',
  `urlInicio` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_rol` WRITE;
/*!40000 ALTER TABLE `ci_rol` DISABLE KEYS */;

INSERT INTO `ci_rol` (`idRol`, `nombre`, `codigo`, `urlInicio`, `indHabilitado`)
VALUES
	(1,'SuperAdmin','S','superadmin/dashboard',1),
	(2,'Admin','A','admin/dashboard',1),
	(3,'Usuario','U','usuario/dashboard',1);

/*!40000 ALTER TABLE `ci_rol` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) NOT NULL DEFAULT '0',
  `user_data` mediumtext NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('032a9c62c5da815f2497c31176741333','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541779634,'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";}'),
	('17ec024e42217adcc4d987e5f127e4f2','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541779724,'a:11:{s:9:\"user_data\";s:0:\"\";s:5:\"us_id\";s:1:\"1\";s:9:\"us_nombre\";s:5:\"Admin\";s:11:\"us_apellido\";s:8:\"ciberdix\";s:6:\"us_rol\";s:1:\"2\";s:9:\"us_avatar\";s:68:\"http://localhost/directrix/assets/uploads/avatar/ciberdix-circle.jpg\";s:8:\"us_email\";s:18:\"admin@ciberdix.com\";s:10:\"rol_codigo\";s:1:\"A\";s:7:\"rol_url\";s:15:\"admin/dashboard\";s:6:\"rol_id\";s:1:\"2\";s:12:\"is_logged_in\";b:1;}'),
	('4df0beef86636adb0fb75a19a9b0335f','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541773279,'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";}'),
	('9001009d516c2182fec55a87182ef5be','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541779688,'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"U\";s:7:\"rol_url\";s:17:\"usuario/dashboard\";s:6:\"rol_id\";s:1:\"3\";}'),
	('c64a230999964286f373130bf8899290','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541779659,'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"U\";s:7:\"rol_url\";s:17:\"usuario/dashboard\";s:6:\"rol_id\";s:1:\"3\";}'),
	('fd852a9270f2e88fb09151ed82494424','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541779715,'a:4:{s:9:\"user_data\";s:0:\"\";s:10:\"rol_codigo\";s:1:\"A\";s:7:\"rol_url\";s:15:\"admin/dashboard\";s:6:\"rol_id\";s:1:\"2\";}');

/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla ci_usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_usuario`;

CREATE TABLE `ci_usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `tipoDocumento` char(2) DEFAULT NULL COMMENT 'CC : CedulaCiudadania - NI : Nit - CE : CedulaExtrangeria',
  `numDocumento` varchar(30) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `desde` datetime DEFAULT NULL,
  `ultimoAcceso` datetime DEFAULT NULL,
  `idRol` int(11) NOT NULL COMMENT 'A:Administrador - I:Ingeniero - M:Maestro',
  `urlAvatar` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Habilitado - 0:Deshabilitado',
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_usuario` WRITE;
/*!40000 ALTER TABLE `ci_usuario` DISABLE KEYS */;

INSERT INTO `ci_usuario` (`idUsuario`, `email`, `contrasena`, `tipoDocumento`, `numDocumento`, `nombre`, `apellido`, `direccion`, `telefono`, `celular`, `desde`, `ultimoAcceso`, `idRol`, `urlAvatar`, `indHabilitado`)
VALUES
	(1,'admin@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055','CC','123','Admin','ciberdix',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28',2,'ciberdix-circle.jpg',1),
	(2,'superadmin@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055','CC','456','SuperAdmin','ciberdix',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28',1,'ciberdix-circle.jpg',1),
	(3,'usuario@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055','CC','789','Usuario','ciberdix',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28',3,'36d35-logo-02.png',1);

/*!40000 ALTER TABLE `ci_usuario` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
