# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.31-MariaDB)
# Base de datos: directrix
# Tiempo de Generación: 2018-11-10 18:40:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla ci_cookies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_cookies`;

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla ci_rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_rol`;

CREATE TABLE `ci_rol` (
  `idRol` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `codigo` char(1) NOT NULL DEFAULT '',
  `urlInicio` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_rol` WRITE;
/*!40000 ALTER TABLE `ci_rol` DISABLE KEYS */;

INSERT INTO `ci_rol` (`idRol`, `nombre`, `codigo`, `urlInicio`, `indHabilitado`)
VALUES
	(1,'SuperAdmin','S','superadmin/dashboard',1),
	(2,'Admin','A','admin/dashboard',1),
	(3,'Usuario','U','usuario/dashboard',1);

/*!40000 ALTER TABLE `ci_rol` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) NOT NULL DEFAULT '0',
  `user_data` mediumtext NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('4adc5316e72beb4c0c7dc705a69f3abe','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541867563,'a:11:{s:9:\"user_data\";s:0:\"\";s:5:\"us_id\";s:1:\"2\";s:9:\"us_nombre\";s:10:\"SuperAdmin\";s:11:\"us_apellido\";s:8:\"ciberdix\";s:6:\"us_rol\";s:1:\"1\";s:9:\"us_avatar\";s:68:\"http://localhost/directrix/assets/uploads/avatar/ciberdix-circle.jpg\";s:8:\"us_email\";s:23:\"superadmin@ciberdix.com\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";s:12:\"is_logged_in\";b:1;}'),
	('b300eecbf2d823c1590904af106d4957','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',1541859493,'a:11:{s:9:\"user_data\";s:0:\"\";s:5:\"us_id\";s:1:\"2\";s:9:\"us_nombre\";s:10:\"SuperAdmin\";s:11:\"us_apellido\";s:8:\"ciberdix\";s:6:\"us_rol\";s:1:\"1\";s:9:\"us_avatar\";s:68:\"http://localhost/directrix/assets/uploads/avatar/ciberdix-circle.jpg\";s:8:\"us_email\";s:23:\"superadmin@ciberdix.com\";s:10:\"rol_codigo\";s:1:\"S\";s:7:\"rol_url\";s:20:\"superadmin/dashboard\";s:6:\"rol_id\";s:1:\"1\";s:12:\"is_logged_in\";b:1;}');

/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla ci_usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_usuario`;

CREATE TABLE `ci_usuario` (
  `idUsuario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `tipoDocumento` char(2) DEFAULT NULL COMMENT 'CC : CedulaCiudadania - NI : Nit - CE : CedulaExtrangeria',
  `numDocumento` varchar(30) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `desde` datetime DEFAULT NULL,
  `ultimoAcceso` datetime DEFAULT NULL,
  `idRol` int(11) unsigned NOT NULL COMMENT 'A:Administrador - I:Ingeniero - M:Maestro',
  `urlAvatar` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Habilitado - 0:Deshabilitado',
  PRIMARY KEY (`idUsuario`),
  KEY `fk_idRol_idRol` (`idRol`),
  CONSTRAINT `fk_idRol_idRol` FOREIGN KEY (`idRol`) REFERENCES `ci_rol` (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ci_usuario` WRITE;
/*!40000 ALTER TABLE `ci_usuario` DISABLE KEYS */;

INSERT INTO `ci_usuario` (`idUsuario`, `email`, `contrasena`, `tipoDocumento`, `numDocumento`, `nombre`, `apellido`, `direccion`, `telefono`, `celular`, `desde`, `ultimoAcceso`, `idRol`, `urlAvatar`, `indHabilitado`)
VALUES
	(1,'admin@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055','CC','123','Admin','ciberdix',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28',2,'ciberdix-circle.jpg',1),
	(2,'superadmin@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055','CC','456','SuperAdmin','ciberdix',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28',1,'ciberdix-circle.jpg',1),
	(3,'usuario@ciberdix.com','81dc9bdb52d04dc20036dbd8313ed055','CC','789','Usuario','ciberdix',NULL,NULL,NULL,'0000-00-00 00:00:00','2016-05-08 12:39:28',3,'36d35-logo-02.png',1);

/*!40000 ALTER TABLE `ci_usuario` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dx_componente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dx_componente`;

CREATE TABLE `dx_componente` (
  `idComponente` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idProceso` int(11) unsigned NOT NULL,
  `codigo` varchar(255) NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idComponente`),
  KEY `idProceso` (`idProceso`),
  CONSTRAINT `dx_componente_ibfk_1` FOREIGN KEY (`idProceso`) REFERENCES `dx_proceso` (`idProceso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dx_componente` WRITE;
/*!40000 ALTER TABLE `dx_componente` DISABLE KEYS */;

INSERT INTO `dx_componente` (`idComponente`, `idProceso`, `codigo`, `nombre`, `indHabilitado`)
VALUES
	(1,1,'GER','Gerencia',1),
	(2,1,'CAL','Calidad',1),
	(3,2,'PROCA','Proyectos y Calidad',1),
	(4,2,'DIDES','Diseño y Desarrollo',1),
	(5,2,'SOMA','Soporte y Mantenimiento',1),
	(6,2,'MERVEN','Mercadeo y Ventas',1);

/*!40000 ALTER TABLE `dx_componente` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dx_documento
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dx_documento`;

CREATE TABLE `dx_documento` (
  `idDocumento` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idTipoDocumento` int(11) unsigned NOT NULL,
  `idComponente` int(11) unsigned NOT NULL,
  `codigo` varchar(255) NOT NULL DEFAULT '',
  `titulo` varchar(255) NOT NULL DEFAULT '',
  `fechaHora` timestamp NULL DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idDocumento`),
  KEY `dx_documento_ibfk_2` (`idComponente`),
  CONSTRAINT `dx_documento_ibfk_1` FOREIGN KEY (`idDocumento`) REFERENCES `dx_tipodocumento` (`idTipoDocumento`),
  CONSTRAINT `dx_documento_ibfk_2` FOREIGN KEY (`idComponente`) REFERENCES `dx_componente` (`idComponente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dx_documento` WRITE;
/*!40000 ALTER TABLE `dx_documento` DISABLE KEYS */;

INSERT INTO `dx_documento` (`idDocumento`, `idTipoDocumento`, `idComponente`, `codigo`, `titulo`, `fechaHora`, `indHabilitado`)
VALUES
	(1,1,4,'1','Guia de desarrollo','2018-11-10 10:29:57',1);

/*!40000 ALTER TABLE `dx_documento` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dx_proceso
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dx_proceso`;

CREATE TABLE `dx_proceso` (
  `idProceso` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idProceso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dx_proceso` WRITE;
/*!40000 ALTER TABLE `dx_proceso` DISABLE KEYS */;

INSERT INTO `dx_proceso` (`idProceso`, `codigo`, `nombre`, `indHabilitado`)
VALUES
	(1,'GES','Proceso de Gestión',1),
	(2,'MIS','Proceso Misionales',1),
	(3,'APO','Proceso de Apoyo',1);

/*!40000 ALTER TABLE `dx_proceso` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dx_tipodocumento
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dx_tipodocumento`;

CREATE TABLE `dx_tipodocumento` (
  `idTipoDocumento` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `nombre` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `orden` tinyint(2) NOT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idTipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dx_tipodocumento` WRITE;
/*!40000 ALTER TABLE `dx_tipodocumento` DISABLE KEYS */;

INSERT INTO `dx_tipodocumento` (`idTipoDocumento`, `codigo`, `nombre`, `orden`, `indHabilitado`)
VALUES
	(1,'M','Manual',1,1),
	(2,'G','Guia',2,1),
	(3,'P','Plantilla',3,1);

/*!40000 ALTER TABLE `dx_tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dx_versiondocumento
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dx_versiondocumento`;

CREATE TABLE `dx_versiondocumento` (
  `idVersionDocumento` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idDocumento` int(11) unsigned NOT NULL,
  `idUsuario` int(11) unsigned NOT NULL,
  `version` varchar(255) NOT NULL DEFAULT '',
  `urlOpen` varchar(255) DEFAULT NULL,
  `urlPdf` varchar(255) DEFAULT NULL,
  `fechaHora` timestamp NULL DEFAULT NULL,
  `indHabilitado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idVersionDocumento`),
  KEY `idDocumento` (`idDocumento`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `dx_versiondocumento_ibfk_1` FOREIGN KEY (`idDocumento`) REFERENCES `dx_documento` (`idDocumento`),
  CONSTRAINT `dx_versiondocumento_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `ci_usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
