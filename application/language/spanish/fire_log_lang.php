<?php

$lang['fire_log_home'] = 'Volver a Hoy';
$lang['fire_log_view'] = 'Ver Archivo';
$lang['fire_log_delete'] = 'Eliminar archivo';
$lang['fire_log_today'] = 'Hoy';
$lang['fire_log_not_found'] = 'No pude encontrar, %log_file%.';
$lang['fire_log_file_deleted'] = 'Log eliminado correctamente ';
$lang['fire_log_show_debug'] = 'Mostrar mensajes de depuración';
$lang['fire_log_show_info'] = 'Mostrar mensajes Info';
$lang['fire_log_show_error'] = 'Mostrar mensajes de error';
$lang['fire_log_show_all'] = 'Mostrar todo';
$lang['fire_log_no_results_found'] = 'No hubo resultados que coinciden con su búsqueda.';