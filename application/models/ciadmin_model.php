<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Modelo base para Codeigniter
 * Versión : 0.7.2
 * Autor : Ciberdix - CarlosPinedat
 *
 *
 * https://gist.github.com/CarlosPinedaT/883ebdb45b0ba294e9122f262f3e8ae5
 */

/**
 *  -- CAMBIOS --
 *
 * v0.7.1 @date 11/11/2018
 * - select_all : cambio de orden de los argumentos
 * - add algunos @retun que faltaban
 *
 * v0.7.2 @date 21/11/2018
 * - ADD select_all_array : se crea nueva función que contiene where a traves de un array
 */
class Ciadmin_model extends CI_Model
{
    /**
     * Selecciona un array registros deacuerdo a los criterios
     * @param String $tabla
     * @param String $orderby
     * @param String $order
     * @param String $whereid
     * @param String $wherevalue
     * @return Array / False
     */
    function select_all($tabla, $whereid = '', $wherevalue = '', $orderby = '', $order = 'ASC')
    {
        if ($orderby != '') {
            $this->db->order_by($orderby, $order);
        }
        if ($whereid != '') {
            $this->db->where($whereid, $wherevalue);
        }
        $query = $this->db->get($tabla);
        return $query->result();
    }

    /**
     * Selecciona una array deceurdo a los criterios de un array
     * @param $tabla
     * @param Array $where_array : array('name !=' => $name, 'id <' => $id, 'date >' => $date);
     * @return mixed
     */
    function select_all_array($tabla, $where_array = '', $orderby = '', $order = 'ASC')
    {
        if ($where_array != '') {
            $this->db->where($where_array);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, $order);
        }
        $query = $this->db->get($tabla);
        return $query->result();
    }

    /**
     * Selecciona un registro deacuerdo a los criterios
     * @param String $tabla
     * @param String $campo
     * @param Int $id
     * @return Array / False
     */
    function select_by_id($tabla, $campo, $id)
    {
        $this->db->where($campo, $id);
        $query = $this->db->get($tabla);
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Actauliza un registro de acuerdo a los criterios
     * @param String $tabla
     * @param Array $datos
     * @param String $campo
     * @param Int $id
     * @return Array / False
     */
    function update_by_id($tabla, $datos, $campo, $id)
    {
        $this->db->where($campo, $id);
        return $this->db->update($tabla, $datos);
    }

    /**
     * Devuelve el total de registro con base en los criterios del where
     * @param String $tabla
     * @param Array $where_array : array('name !=' => $name, 'id <' => $id, 'date >' => $date);
     * @param String $whereValue
     * @return Int
     */
    public function total_registros($tabla, $where_array = '')
    {
        $this->db->from($tabla);
        if ($where_array != '') {
            $this->db->where($where_array);
        }
        return $this->db->count_all_results();
    }

    /**
     * Insertar registro en una tabla determinada
     * @param String $tabla
     * @param Array $data
     * @param Int $returnId
     * @return true/false
     */
    public function insertar_registro($tabla, $data, $returnId = 0)
    {
        $return = $this->db->insert($tabla, $data);
        if ($returnId === 1) {
            $return = $this->db->insert_id();
        }
        return $return;
    }

    /**
     * Eliminar un registro de una tabla determinada
     * @param String $tabla
     * @param String $whereid
     * @param String $wherevalue
     * @return true/false
     */
    public function eliminar_registro($tabla, $whereid, $wherevalue)
    {
        $this->db->where($whereid, $wherevalue);
        return $this->db->delete($tabla);
    }
}
