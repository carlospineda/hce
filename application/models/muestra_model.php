<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Muestra_model extends CI_Model
{


    function info($idMuestra){
        /**
        SELECT
        hce.hc_tercero.primerNombre,
        hce.hc_tercero.segundoNombre,
        hce.hc_tercero.primerApellido,
        hce.hc_tercero.segundoApellido,
        hce.hc_tipodocumento.codTipoDocumento,
        hce.hc_tipodocumento.tipoDocumento,
        hce.hc_tercero.numeroDocumento,
        hce.hc_tercero.fechaNacimiento,
        hce.hc_muestra.fechaHora,
        hce.hc_muestra.estado,
        hce.ci_usuario.nombre,
        hce.ci_usuario.apellido
        FROM
        hce.hc_muestra
        JOIN hce.hc_tercero
        ON hce.hc_muestra.idTercero = hce.hc_tercero.idTercero
        JOIN hce.ci_usuario
        ON hce.hc_muestra.idUsuario = hce.ci_usuario.idUsuario
        JOIN hce.hc_tipodocumento
        ON hce.hc_tercero.idTipoDocumento = hce.hc_tipodocumento.idTipoDocumento
        WHERE
        idMuestra = 1
         */

        $this->db->select('hc_tercero.primerNombre,
        hc_tercero.segundoNombre,
        hc_tercero.primerApellido,
        hc_tercero.segundoApellido,
        hc_tipodocumento.codTipoDocumento,
        hc_tipodocumento.tipoDocumento,
        hc_tercero.numeroDocumento,
        hc_tercero.fechaNacimiento,
        hc_muestra.fechaHora,
        hc_muestra.estado,
        ci_usuario.nombre,
        ci_usuario.apellido');
        $this->db->from('hc_muestra');
        $this->db->join('hc_tercero', 'hc_muestra.idTercero = hc_tercero.idTercero');
        $this->db->join('ci_usuario', 'hc_muestra.idUsuario = ci_usuario.idUsuario');
        $this->db->join('hc_tipodocumento', 'hc_tercero.idTipoDocumento = hc_tipodocumento.idTipoDocumento');
        $this->db->where('hc_muestra.idMuestra', $idMuestra);

        $query = $this->db->get();
        return $query->row();
    }

}
