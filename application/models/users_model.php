<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

    /**
     * Valida el inicio de sesion en la base de datos
     * @param string $user_name
     * @param string $password
     * 
     * @return void
     */
    function validate($user_name, $password) {
        $this->db->where('email', $user_name);
        $this->db->where('contrasena', $password);
        $query = $this->db->get('ci_usuario');

        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    /**
     * Serializar los datos de sesión almacenados en la base de datos, 
     * Almacenarlo en el nuevo Array y devolverlo al controlador
     * 
     * @return array
     */
    function get_db_session_data() {
        $query = $this->db->select('user_data')->get('ci_sessions');
        $user = array(); /* matriz para almacenar los datos de usuario buscamos a */
        foreach ($query->result() as $row) {
            $udata = unserialize($row->user_data);
            /* put data in array using username as key */
            $user['user_name'] = $udata['user_name'];
            $user['is_logged_in'] = $udata['is_logged_in'];
        }
        return $user;
    }

    /**
     * Obtenemos todos los datos del Usuario
     * @param string $idUsuario
     * 
     * @return array
     */
    function get_perfil_usuario($idUsuario) {
        $this->db->select('*');
        $this->db->from('ci_usuario');
        $this->db->where('idUsuario', $idUsuario);
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Actualiza la tabla Usuario
     * @param type $idUsuario
     * @param type $datos
     * 
     * @return int resultado de la operación del update (1-success, 0-error)
     */
    function actualizar_usuario($idUsuario, $datos) {
        $this->db->where('idUsuario', $idUsuario);
        return $this->db->update('ci_usuario', $datos);
    }

    /**
     * Actualiza avatar usuario
     * @param int $user_id Id unico del usuario
     * 
     * @return string Nombre imagen avatar
     */
    function update_profile_pic($user_id) {
        //show($user_id);
        if ($_FILES['avatar']['error'] == 0) {
            $relative_url = $this->upload->file_name;
            $profile_data['urlAvatar'] = $relative_url;
        }
        $this->db->where('idUsuario', $user_id);
        $this->db->update('ci_usuario', $profile_data);

        return $relative_url;
    }

}

