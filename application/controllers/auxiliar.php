<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auxiliar extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in') || $this->session->userdata('rol_codigo') != 'X') {
            redirect('login');
        }
    }

    public function index()
    {
        redirect($this->session->userdata('rol_url'));
    }

    public function dashboard()
    {
        $output = '';

        $output['totalTerceros'] = $this->Ciadmin_model->total_registros('hc_tercero');
        $output['totalMuestras'] = $this->Ciadmin_model->total_registros('hc_muestra');

        $datas['output'] = $this->load->view('auxiliar/dashboard', $output, TRUE);
        $this->template_output($datas);
    }


    public function pacientes()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_tercero')
            ->set_subject('Pacientes')
            ->columns('idTipoDocumento', 'numeroDocumento', 'Nombre', 'idGenero')
            ->set_relation('idTipoDocumento', 'hc_tipodocumento', 'codTipoDocumento', array('indHabilitado' => 1))
            ->set_relation('idGenero', 'hc_genero', 'genero', array('indHabilitado' => 1))
            ->set_relation('idEstadoCivil', 'hc_estadocivil', 'estadoCivil', array('indHabilitado' => 1))
            ->set_relation('idFactorRh', 'hc_factorrh', 'factor', array('indHabilitado' => 1))
            ->set_relation('idGenero', 'hc_genero', 'genero', array('indHabilitado' => 1))
            ->set_primary_key('idCiudad', 'v_ciudad')
            ->set_relation('idCiudadNacimiento', 'v_ciudad', '{nombre} - {nomDepartamento}', array('indHabilitado' => 1))
            ->set_relation('idCiudadExpDocumento', 'v_ciudad', '{nombre} - {nomDepartamento}', array('indHabilitado' => 1))
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'))
            ->callback_column('Nombre', array($this, 'callback_fullname'))
            ->callback_after_insert(array($this, 'callback_insert_paciente'))
            ->field_type('uuid', 'readonly')
            ->field_type('uidEhr', 'readonly')
            ->field_type('fechaCreacion', 'readonly')
            ->unset_clone()
            ->display_as('primerNombre', 'Primer Nombre')
            ->display_as('segundoNombre', 'Segundo Nombre')
            ->display_as('primerApellido', 'Primer Apellido')
            ->display_as('segundoApellido', 'Segundo Apellido')
            ->display_as('idTipoDocumento', 'Tipo de Documento')
            ->display_as('numeroDocumento', 'Número de Documento')
            ->display_as('fechaDocumento', 'Fecha Exp')
            ->display_as('idCiudadExpDocumento', 'Ciudad Exp Documento')
            ->display_as('fechaNacimiento', 'Fecha de Nacimiento')
            ->display_as('idCiudadNacimiento', 'Lugar de Nacimiento')
            ->display_as('idGenero', 'Sexo')
            ->display_as('idEstadoCivil', 'Estado Civil')
            ->display_as('idFactorRh', 'Factor RH')
            ->display_as('fechaCreacion', 'Fecha de Creación');

        $output = $crud->render();
        $this->template_output($output);
    }

    public function muestra()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_muestra')
            ->set_subject('Citologia')
            ->columns(['idTercero','fechaHora','estado','indHabilitado'])
            ->set_relation('idTercero', 'hc_tercero', '{numeroDocumento} {primerNombre} {segundoNombre} {primerApellido} {segundoApellido}')
            // ->set_relation('idUsuario', 'ci_usuario', '{nombre} {apellido}')
            ->field_type('idUsuario', 'hidden', $this->session->userdata('us_id'))
            ->field_type('estado', 'hidden', 'I')
            ->where('estado', 'I')

            ->display_as('idTercero', 'Paciente')

        ;

        $where_array = array(
            'idMuestra' => $this->uri->segment(count($this->uri->segments))
        );
        $nRegistros = $this->Ciadmin_model->total_registros('hc_muestraantecedente', $where_array);

        if ($nRegistros == 0) {
            $addUrl = 'add';
        } else {
            $addUrl = 'edit/1';
        }

        $crud->set_lang_string('update_success_message',
            'Muestra guardada con Exito.<br/>Un momento mientras lo dirigimos a agregar antecedentes
            <script type="text/javascript">
             window.location = "' . base_url('auxiliar/agregarantecedente/' . $this->uri->segment(count($this->uri->segments))) . '/' . $addUrl . '";
		 </script>
		 <div style="display:none">
		 '
        );

        $crud->set_lang_string('insert_success_message',
            'Muestra guardada con Exito.<br/>Un momento mientras lo dirigimos a agregar antecedentes
            <script type="text/javascript">
             window.location = "' . base_url('auxiliar/agregarantecedente/' . $this->uri->segment(count($this->uri->segments))) . '/' . $addUrl . '";
		 </script>
		 <div style="display:none">
		 '
        );

        $output = $crud->render();
        $this->template_output($output);
    }

    public function agregarantecedente()
    {

        $this->load->model('muestra_model');
        $idMuestra = $this->uri->segment(3);

        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_muestraantecedente')
            ->set_subject('Ancedenete de la muestra')
            ->where('idMuestra', $idMuestra)
            ->field_type('idMuestra', 'hidden', $idMuestra)
            ->field_type('indActivoSexual', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('indGestacion', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('indPartoVaginal', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('indCesarea', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('indAborto', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('cicloMestrual', 'dropdown', array('R' => 'Regular', 'I' => 'Irregular'))
            ->field_type('embarazada', 'dropdown', array('S' => 'Si', 'N' => 'No', 'X' => 'No Sabe'))
            ->field_type('indCitologiaPrevia', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('resultadoCitologiaPrevia', 'dropdown', array("N" => "Normal", "A" => "Anormal"))
            ->field_type('indPanifica', 'true_false', array("0" => "No", "1" => "Si"))
            ->field_type('metodoPlanificacion', 'dropdown', array("L" => "Ligadura", "P" => "Píldora", "R" => "Preservativo", "B" => "Barras", "D" => "DIU", "N" => "Natural", "I" => "Inyección"))
            ->field_type('procedimientosAnteriores', 'dropdown', array("N" => "Ninguno", "C" => "Cauterizado", "H" => "Histerectomía", "O" => "Cotización", "R" => "Radiación", "T" => "Otro"))
            ->field_type('aspectoCuello', 'dropdown', array("A" => "Ausente", "S" => "Sano", "L" => "Lesión Visible", "T" => "Atrófico", "N" => "Sangrante", "Q" => "Quiste de Naboth", "P" => "Pólipo", "C" => "Congestivo", "U" => "Ulcerado", "E" => "Erosionado"))

            ->display_as('indActivoSexual','Activo Sexualmente?')
            ->display_as('fechaUltimaRelacionSexual', 'Fecha Ultima Relación Sexual?')
            ->display_as('indGestacion', 'Gestación?')
            ->display_as('indPartoVaginal', 'Parto Vaginal?')
            ->display_as('indCesarea', 'Cesarea?')
            ->display_as('indAborto', 'Aborto?')
            ->display_as('numeroHijosVivos', 'Numero de Hijos Vivos')
            ->display_as('fechaUltimaRegla', 'Fecha de la Última Regla')
            ->display_as('cicloMestrual', 'Ciclo Mestrual')
            ->display_as('embarazada', 'Embarazada?')
            ->display_as('indCitologiaPrevia', 'Citologia Previa')
            ->display_as('resultadoCitologiaPrevia', 'Resultado de la Citologia Previa')
            ->display_as('indPanifica', 'Actualmente Planifica?')
            ->display_as('metodoPlanificacion', 'Que Metodo de Planificacción?')
            ->display_as('procedimientosAnteriores', 'Procedimientos anteriores en cuello uterino')
            ->display_as('aspectoCuello', 'Aspecto del Cuello')

            ->callback_after_insert(array($this, 'callback_agregarantecedente'))
            ->callback_after_update(array($this, 'callback_agregarantecedente'))

        ;

        $crud->set_lang_string('update_success_message',
            'Muestra guardada con Exito.<br/>Un momento mientras lo dirigimos a agregar antecedentes
            <script type="text/javascript">
             window.location = "'.base_url().'auxiliar/muestra";
		 </script>
		 <div style="display:none">
		 ');

        $crud->set_lang_string('insert_success_message',
            'Muestra guardada con Exito.<br/>Un momento mientras lo dirigimos a agregar antecedentes
            <script type="text/javascript">
             window.location = "'.base_url().'auxiliar/muestra";
		 </script>
		 <div style="display:none">
		 ');


        $output = $crud->render();

        $info = $this->muestra_model->info($idMuestra);
        // test($info, true);
        //$html_top_b = '<div style="text-align: center;"><a href="'.base_url().'auxiliar/cargarmuestra/'. $idMuestra.'" class="btn btn-primary">Agregar Imagenes a la Muestras</a> </div>';
        $html_top_b = '';

        $output->output_top = $this->load->view('auxiliar/muestrainfo_view', $info, true);
        $output->output_top_b = $html_top_b;
        $output->output_botton = $this->load->view('auxiliar/footer_muestraantecedente_view', $info, true);


        $this->template_output($output);
    }

    public function listacargarmuestra(){
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_muestra')
            ->set_subject('Muestra')
            ->columns(['idTercero','fechaHora','estado','indHabilitado'])
            ->set_relation('idTercero', 'hc_tercero', '{numeroDocumento} {primerNombre} {segundoNombre} {primerApellido} {segundoApellido}')
            // ->set_relation('idUsuario', 'ci_usuario', '{nombre} {apellido}')
            ->field_type('idUsuario', 'hidden', $this->session->userdata('us_id'))
            ->where('estado', 'A')

            ->display_as('idTercero', 'Paciente')
            ->unset_add()
            ->unset_edit()
            ->unset_delete()
            ->unset_read()
            ->add_action('Agregar Imagen Muestra', '', 'auxiliar/cargarmuestra','ui-icon-photo');

        ;
        $output = $crud->render();
        $this->template_output($output);
    }

    public function cargarmuestra()
    {

        $this->load->model('muestra_model');
        $idMuestra = $this->uri->segment(3);

        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_muestraimagen')
            ->set_subject('Imagenes de la Muestra')
            ->set_field_upload('urlImagen', 'assets/uploads/muestras')
            ->where('idMuestra', $idMuestra)
            ->field_type('idMuestra', 'hidden', $idMuestra);


        $output = $crud->render();

        $info = $this->muestra_model->info($idMuestra);
        // test($info, true);

        $output->output_top = $this->load->view('auxiliar/muestrainfo_view', $info, true);

        $output->output_botton='<a href="'.base_url().'auxiliar/cambiarestado/'.$idMuestra.'/M" class="btn btn-block btn-primary">Finalizar Carga de imagenes</a>';

        $this->template_output($output);
    }

    public function cambiarestado(){
        $idMuestra = $this->uri->segment(3);
        $newEstado = $this->uri->segment(4);

        //test($idMuestra);
        //test($newEstado, true);

        $data = array(
            "estado" => $newEstado
        );

        $this->Ciadmin_model->update_by_id('hc_muestra', $data, 'idMuestra', $idMuestra);

        redirect(base_url()."auxiliar/listacargarmuestra");


    }



    /*
     * -----------------------------------------------------------------------------
     *  Funciones Globales 
     * -----------------------------------------------------------------------------
     */

    /**
     * Función que imprime en la plantilla el resultante del controller
     * @param null $output
     */
    private function template_output($output = null)
    {
        $rolActual = $this->uri->segment(1);

        if (is_array($output)) {
            $output['rol_actual'] = $rolActual;
        } else {
            $output->rol_actual = $rolActual;
        }

        $this->load->view('template/template.php', $output);
    }

    /**
     * Encripta el valor de texto en un esquema de MD5
     * @param $post_array
     * @param null $primary_key
     * @return mixed
     */
    function encrypt_password_callback($post_array, $primary_key = null)
    {
        $this->load->helper('security');
        $post_array['contrasena'] = do_hash($post_array['contrasena'], 'md5');
        return $post_array;
    }

    /**
     * Imprime icono para el datatable enable/disable
     * @param $value
     * @param $row
     * @return string
     */
    function callback_indhabilitado($value, $row)
    {
        $icon = 'fa-check-square';
        $class = 'text-success';
        if ($value == '0') {
            $icon = 'fa-square-o';
            $class = '';
        }
        return '<div class="text-hide">' . $value . '</div><div class="text-center ' . $class . '"> <i class="fa fa-lg ' . $icon . '"></i> </div>';
    }

    /**
     * Genera el valor en un badge
     * @param $value
     * @param $row
     * @return string
     */
    function callback_badge($value, $row)
    {
        return '<div class="text-center"><span class="badge">' . $value . '</span></div>';
    }

    /**
     * Concatena el nombre en un solo string
     * @param $value
     * @param $row
     * @return string
     */
    function callback_fullname($value, $row)
    {
        $fullname = '';
        if ($row->primerNombre <> '') $fullname .= $row->primerNombre;
        if ($row->segundoNombre <> '') $fullname = $fullname . ' ' . $row->segundoNombre;
        if ($row->primerApellido <> '') $fullname = $fullname . ' ' . $row->primerApellido;
        if ($row->segundoApellido <> '') $fullname = $fullname . ' ' . $row->segundoApellido;

        if ($row->fechaNacimiento <> NULL) {
            $anios = calcularAnios($row->fechaNacimiento);
            $fullname = $fullname . '<span class="badge" style="float: right">' . $anios . ' años</span>';
        }

        return ucwords(strtolower($fullname));
    }

    /**
     * Agrega un UUID unico a cada Tercero y fecha de creación
     * @param $post_array
     * @return mixed
     */
    function callback_insert_paciente($post_array, $primary_key)
    {

        $uuid = generarUuid();
        $datos = array(
            'uuid' => $uuid,
            'fechaCreacion' => ahora()
        );

        $rta = $this->Ciadmin_model->update_by_id('hc_tercero', $datos, 'idTercero', $primary_key);

        if ($rta && $this->config->item('ehrserver_on')) {

            $ehr = $this->ehrserver->create_ehr(trim($uuid));

            if ($ehr) {
                $d = array(
                    'uidEhr' => $ehr->uid
                );

                $rta = $this->Ciadmin_model->update_by_id('hc_tercero', $d, 'idTercero', $primary_key);

                if (!$rta) {
                    test('ERROR :  No se pudo agregar la UID de HC al nuevo Tercero');
                } else {

                    $urlXml = FCPATH . 'assets/openehr/templates/datos_demograficos.es.v1_Instance.xml';
                    $xmlInside = file_get_contents($urlXml);

                    $now = new DateTime('NOW');
                    $iso8601date = $now->format('c'); // ISO8601 formated datetime

                    $fechaNacimiento = new DateTime($post_array['fechaNacimiento']);
                    $fechaNacimiento = date_format($fechaNacimiento, 'c');

                    $genero = $this->Ciadmin_model->select_by_id('hc_genero', 'idGenero', $post_array['idGenero']);

                    $nomCommiter = $this->session->userdata('us_nombre') . ' ' . $this->session->userdata('us_apellido');

                    $dataXml = array(
                        '[[CONTRIBUTION:::UUID]]' => generarUuid(),
                        '[[COMMITTER_ID:::UUID]]' => $this->session->userdata('us_uuid'),
                        '[[COMMITTER_NAME:::STRING]]' => $this->session->userdata('us_nombre') . ' ' . $this->session->userdata('us_apellido'),
                        '[[TIME_COMMITTED:::DATETIME]]' => $iso8601date,
                        '[[VERSION_ID:::VERSION_ID]]' => generarUuid() . '::PHP.TEST::1',
                        '[[COMPOSER_ID:::UUID]]' => generarUuid(),
                        '[[COMPOSER_NAME:::STRING]]' => $nomCommiter,
                        '[[GENERO_VALUE:::STRING]]' => $genero->genero,
                        '[[GENERO_CODE:::STRING]]' => $genero->codOpenEhr,
                        '[[FECHA_NACIMIENTO:::DATETIME]]' => $post_array['fechaNacimiento']
                    );

                    foreach ($dataXml as $clave => $valor) {
                        $xmlInside = str_replace($clave, $valor, $xmlInside);
                    }

                    $res = $this->ehrserver->commit_composition($xmlInside, $ehr->uid, $nomCommiter, 'CBRDX-HCE');
                    if ($res->type <> 'AA') {
                        test('Ocurrio un error en el commit de datos_demograficos.es.v1');
                    } else {
                        ca_growl('HCE', 'Se creó correctamente la historia clínica en el servidor de HCE', 2);
                    }

                }

            } else {
                test('ERROR :  No se pudo agregar la HC al nuevo Tercero');
            }

        } else {
            test('ERROR : No se pudo agregar un UUID al tercero guardado');
        }

        return true;
    }


    /**
     * Agrega UUID y Fecha de creación al auxiliar creado
     * @param $post_array
     * @param $primary_key
     * @return bool
     */
    function callback_insert_usuario($post_array, $primary_key)
    {

        $uuid = generarUuid();
        $datos = array(
            'uuid' => $uuid,
            'desde' => ahora()
        );

        $rta = $this->Ciadmin_model->update_by_id('ci_usuario', $datos, 'idUsuario', $primary_key);

        if ($rta) {
            return true;
        } else {
            test('ERROR : No se pudo agregar un UUID al Usuario guardado');
        }


    }

    function callback_agregarantecedente($post_array,$primary_key)
    {
        //test($post_array);

        $data = array(
            "estado" => 'A',
        );

        $this->Ciadmin_model->update_by_id('hc_muestra', $data, 'idMuestra', $post_array['idMuestra']);

        //($tabla, $datos, $campo, $id)

        return true;
    }

}

// END Superadmin class

/* End of file Superadmin.php */
/* Location: ./application/controllers/superadmin.php */