<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Superadmin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in') || $this->session->userdata('rol_codigo') != 'S') {
            redirect('login');
        }
    }

    public function index()
    {
        redirect($this->session->userdata('rol_url'));
    }

    public function dashboard()
    {
        $output = '';
        $output['total_pacientes'] = $this->Ciadmin_model->total_registros('hc_tercero');
        $output['total_ciudades'] = $this->Ciadmin_model->total_registros('hc_ciudad');
        $output['total_departamentos'] = $this->Ciadmin_model->total_registros('hc_departamento');
        $output['total_paises'] = $this->Ciadmin_model->total_registros('hc_pais');

        $datas['output'] = $this->load->view('superadmin/dashboard', $output, TRUE);
        $this->template_output($datas);
    }

    public function usuarios()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('ci_usuario')
            ->set_subject('Usuario')
            ->columns('nombre', 'apellido', 'idRol', 'email')
            ->required_fields('email', 'contrasena', 'nombre', 'apellido')
            ->set_field_upload('urlAvatar', 'assets/uploads/avatar')
            ->field_type('tipo', 'dropdown', array('S' => 'SuperAdministrador', 'A' => 'Administrador', 'I' => 'Ingeniero', 'M' => 'Maestro'))
            ->field_type('tipoDocumento', 'dropdown', array('CC' => 'Cedula de Ciudadania', 'NI' => 'NIT', 'CE' => 'Cedula de Extrangeria'))
            ->set_relation('idRol', 'ci_rol', 'nombre', array('indHabilitado' => 1))
            ->change_field_type('contrasena', 'password')
            ->field_type('uuid', 'readonly')
            ->field_type('desde', 'readonly')
            ->callback_before_insert(array($this, 'encrypt_password_callback'))
            ->callback_before_update(array($this, 'encrypt_password_callback'))
            ->callback_after_insert(array($this, 'callback_insert_usuario'));

        $output = $crud->render();
        $this->template_output($output);
    }

    public function logs()
    {
        $this->load->helper('spark_url');
        $this->load->helper('fire_log');
        $this->load->config('fire_log');
        $this->load->language('fire_log');
        $data = $this->load->library('fire_log');
    }

    public function roles()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('ci_rol')
            ->set_subject('Rol')
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'))
            ->display_as('codigo','Código');

        $output = $crud->render();
        $this->template_output($output);
    }

    public function pais()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_pais')
            ->set_subject('Pais')
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function departamento()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_departamento')
            ->set_subject('Departamento')
            ->set_relation('idPais', 'hc_pais', 'nombre', array('indHabilitado' => 1))
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function ciudad()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_ciudad')
            ->set_subject('Ciudad')
            ->set_relation('idDepartamento', 'hc_departamento', 'nombre', array('indHabilitado' => 1))
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function estadocivil()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_estadocivil')
            ->set_subject('Estado Civil')
            //->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));
        ;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function factorrh()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_factorrh')
            ->set_subject('Factor RH')
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function genero()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_genero')
            ->set_subject('Género')
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function tipocontacto()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_tipocontacto')
            ->set_subject('Tipo de Contacto')
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function tipodocumento()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_tipodocumento')
            ->set_subject('Tipo de Documento')
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'));;

        $output = $crud->render();
        $this->template_output($output);
    }

    public function pacientes()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_tercero')
            ->set_subject('Pacientes')
            ->columns('idTipoDocumento', 'numeroDocumento', 'Nombre', 'idGenero')
            ->set_relation('idTipoDocumento', 'hc_tipodocumento', 'codTipoDocumento', array('indHabilitado' => 1))
            ->set_relation('idGenero', 'hc_genero', 'genero', array('indHabilitado' => 1))
            ->set_relation('idEstadoCivil', 'hc_estadocivil', 'estadoCivil', array('indHabilitado' => 1))
            ->set_relation('idFactorRh', 'hc_factorrh', 'factor', array('indHabilitado' => 1))
            ->set_relation('idGenero', 'hc_genero', 'genero', array('indHabilitado' => 1))
            ->set_primary_key('idCiudad', 'v_ciudad')
            ->set_relation('idCiudadNacimiento', 'v_ciudad', '{nombre} - {nomDepartamento}', array('indHabilitado' => 1))
            ->set_relation('idCiudadExpDocumento', 'v_ciudad', '{nombre} - {nomDepartamento}', array('indHabilitado' => 1))
            ->callback_column('indHabilitado', array($this, 'callback_indhabilitado'))
            ->callback_column('Nombre', array($this, 'callback_fullname'))
            ->callback_after_insert(array($this, 'callback_insert_paciente'))
            ->field_type('uuid', 'readonly')
            ->field_type('uidEhr', 'readonly')
            ->field_type('fechaCreacion', 'readonly')
            ->unset_clone()
            ->display_as('primerNombre','Primer Nombre')
            ->display_as('segundoNombre', 'Segundo Nombre')
            ->display_as('primerApellido', 'Primer Apellido')
            ->display_as('segundoApellido', 'Segundo Apellido')
            ->display_as('idTipoDocumento', 'Tipo de Documento')
            ->display_as('numeroDocumento', 'Número de Documento')
            ->display_as('fechaDocumento', 'Fecha Exp')
            ->display_as('idCiudadExpDocumento', 'Ciudad Exp Documento')
            ->display_as('fechaNacimiento', 'Fecha de Nacimiento')
            ->display_as('idCiudadNacimiento', 'Lugar de Nacimiento')
            ->display_as('idGenero', 'Sexo')
            ->display_as('idEstadoCivil', 'Estado Civil')
            ->display_as('idFactorRh', 'Factor RH')
            ->display_as('fechaCreacion', 'Fecha de Creación');

        $output = $crud->render();
        $this->template_output($output);
    }



    /*
     * -----------------------------------------------------------------------------
     *  Funciones Globales 
     * -----------------------------------------------------------------------------
     */

    /**
     * Función que imprime en la plantilla el resultante del controller
     * @param null $output
     */
    private function template_output($output = null)
    {
        $rolActual = $this->uri->segment(1);

        if (is_array($output)) {
            $output['rol_actual'] = $rolActual;
        } else {
            $output->rol_actual = $rolActual;
        }

        $this->load->view('template/template.php', $output);
    }

    /**
     * Encripta el valor de texto en un esquema de MD5
     * @param $post_array
     * @param null $primary_key
     * @return mixed
     */
    function encrypt_password_callback($post_array, $primary_key = null)
    {
        $this->load->helper('security');
        $post_array['contrasena'] = do_hash($post_array['contrasena'], 'md5');
        return $post_array;
    }

    /**
     * Imprime icono para el datatable enable/disable
     * @param $value
     * @param $row
     * @return string
     */
    function callback_indhabilitado($value, $row)
    {
        $icon = 'fa-check-square';
        $class = 'text-success';
        if ($value == '0') {
            $icon = 'fa-square-o';
            $class = '';
        }
        return '<div class="text-hide">' . $value . '</div><div class="text-center ' . $class . '"> <i class="fa fa-lg ' . $icon . '"></i> </div>';
    }

    /**
     * Genera el valor en un badge
     * @param $value
     * @param $row
     * @return string
     */
    function callback_badge($value, $row)
    {
        return '<div class="text-center"><span class="badge">' . $value . '</span></div>';
    }

    /**
     * Concatena el nombre en un solo string
     * @param $value
     * @param $row
     * @return string
     */
    function callback_fullname($value, $row)
    {
        $fullname = '';
        if ($row->primerNombre <> '') $fullname .= $row->primerNombre;
        if ($row->segundoNombre <> '') $fullname = $fullname . ' ' . $row->segundoNombre;
        if ($row->primerApellido <> '') $fullname = $fullname . ' ' . $row->primerApellido;
        if ($row->segundoApellido <> '') $fullname = $fullname . ' ' . $row->segundoApellido;

        if ($row->fechaNacimiento <> NULL) {
            $anios = calcularAnios($row->fechaNacimiento);
            $fullname = $fullname . '<span class="badge" style="float: right">' . $anios . ' años</span>';
        }

        return ucwords(strtolower($fullname));
    }

    /**
     * Agrega un UUID unico a cada Tercero y fecha de creación
     * @param $post_array
     * @return mixed
     */
    function callback_insert_paciente($post_array, $primary_key)
    {

        $uuid = generarUuid();
        $datos = array(
            'uuid' => $uuid,
            'fechaCreacion' => ahora()
        );

        $rta = $this->Ciadmin_model->update_by_id('hc_tercero', $datos, 'idTercero', $primary_key);

        if ($rta && $this->config->item('ehrserver_on')) {

            $ehr = $this->ehrserver->create_ehr(trim($uuid));

            if ($ehr) {
                $d = array(
                    'uidEhr' => $ehr->uid
                );

                $rta = $this->Ciadmin_model->update_by_id('hc_tercero', $d, 'idTercero', $primary_key);

                if (!$rta) {
                    test('ERROR :  No se pudo agregar la UID de HC al nuevo Tercero');
                } else {

                    $urlXml = FCPATH . 'assets/openehr/templates/datos_demograficos.es.v1_Instance.xml';
                    $xmlInside = file_get_contents($urlXml);

                    $now = new DateTime('NOW');
                    $iso8601date = $now->format('c'); // ISO8601 formated datetime

                    $fechaNacimiento = new DateTime($post_array['fechaNacimiento']);
                    $fechaNacimiento = date_format($fechaNacimiento, 'c');

                    $genero = $this->Ciadmin_model->select_by_id('hc_genero', 'idGenero', $post_array['idGenero']);

                    $nomCommiter = $this->session->userdata('us_nombre') . ' ' . $this->session->userdata('us_apellido');

                    $dataXml = array(
                        '[[CONTRIBUTION:::UUID]]' => generarUuid(),
                        '[[COMMITTER_ID:::UUID]]' => $this->session->userdata('us_uuid'),
                        '[[COMMITTER_NAME:::STRING]]' => $this->session->userdata('us_nombre') . ' ' . $this->session->userdata('us_apellido'),
                        '[[TIME_COMMITTED:::DATETIME]]' => $iso8601date,
                        '[[VERSION_ID:::VERSION_ID]]' => generarUuid() . '::PHP.TEST::1',
                        '[[COMPOSER_ID:::UUID]]' => generarUuid(),
                        '[[COMPOSER_NAME:::STRING]]' => $nomCommiter,
                        '[[GENERO_VALUE:::STRING]]' => $genero->genero,
                        '[[GENERO_CODE:::STRING]]' => $genero->codOpenEhr,
                        '[[FECHA_NACIMIENTO:::DATETIME]]' => $post_array['fechaNacimiento']
                    );

                    foreach ($dataXml as $clave => $valor) {
                        $xmlInside = str_replace($clave, $valor, $xmlInside);
                    }

                    $res = $this->ehrserver->commit_composition($xmlInside, $ehr->uid, $nomCommiter, 'CBRDX-HCE');
                    if ($res->type <> 'AA') {
                        test('Ocurrio un error en el commit de datos_demograficos.es.v1');
                    } else {
                        ca_growl('HCE', 'Se creó correctamente la historia clínica en el servidor de HCE', 2);
                    }

                }

            } else {
                test('ERROR :  No se pudo agregar la HC al nuevo Tercero');
            }

        } else {
            test('Error : No se pudo agregar un UUID al tercero guardado');
        }

        return true;
    }


    /**
     * Agrega UUID y Fecha de creación al usuario creado
     * @param $post_array
     * @param $primary_key
     * @return bool
     */
    function callback_insert_usuario($post_array, $primary_key)
    {

        $uuid = generarUuid();
        $datos = array(
            'uuid' => $uuid,
            'desde' => ahora()
        );

        $rta = $this->Ciadmin_model->update_by_id('ci_usuario', $datos, 'idUsuario', $primary_key);

        if ($rta) {
            return true;
        } else {
            test('Error : No se pudo agregar un UUID al Usuario guardado');
        }


    }

}

// END Superadmin class

/* End of file Superadmin.php */
/* Location: ./application/controllers/superadmin.php */