<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Citotecnologo extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in') || $this->session->userdata('rol_codigo') != 'C') {
            redirect('login');
        }
    }

    public function index()
    {
        redirect($this->session->userdata('rol_url'));
    }

    public function dashboard()
    {
        $output = '';

        $tmuestra = array(
            'estado' => 'M'
        );

        $output['totalTerceros'] = $this->Ciadmin_model->total_registros('hc_tercero');
        $output['totalMuestras'] = $this->Ciadmin_model->total_registros('hc_muestra', $tmuestra);

        $datas['output'] = $this->load->view('citotecnologo/dashboard', $output, TRUE);
        $this->template_output($datas);
    }




    public function muestra()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('hc_muestra')
            ->set_subject('Citologia')
            ->set_relation('idUsuario', 'ci_usuario', '{nombre} {apellido}')
            ->set_relation('idTercero', 'hc_tercero', '{numeroDocumento} {primerNombre} {segundoNombre} {primerApellido} {segundoApellido}')
            ->field_type('idUsuario', 'hidden', $this->session->userdata('us_id'))
            ->field_type('estado', 'hidden', 'A')
            ->where('estado', 'M')
            ->unset_add()
            ->unset_edit()
            ->unset_read()
            ->unset_delete()
            ->unset_clone()
            ->add_action('Ver Muestra', '', 'citotecnologo/vermuestra', 'ui-icon-image');
        $output = $crud->render();
        $this->template_output($output);
    }


    public function vermuestra()
    {
        $this->load->model('muestra_model');
        $idMuestra = $this->uri->segment(3);

        $output['idMuestra'] = $idMuestra;
        $infMuestra = $this->muestra_model->info($idMuestra);
        $imgMuestra = $this->Ciadmin_model->select_all('hc_muestraimagen', 'idMuestra', $idMuestra);
        $antMuestra = $this->Ciadmin_model->select_all('hc_muestraantecedente', 'idMuestra', $idMuestra);

        //test($output, true);

        if($infMuestra) $output['infMuestra'] = $infMuestra;
        if($imgMuestra) $output['imgMuestra'] = $imgMuestra;
        if($antMuestra) $output['antMuestra'] = $antMuestra;


        $datas['output'] = $this->load->view('citotecnologo/vermuestra_view', $output, TRUE);
        $this->template_output($datas);
    }

    public function guardarDiagnostico()
    {
        // test($this->input->post(), true);
        /*
            [slcTipoMuestra] => a
            [slcCalidadMuestra] => a
            [slcCategorizacionGeneral] => a
            [slcCategorizacionGeneralEspecifique] => a
            [slcVariacionesNoNeo] => a
            [slcCambiosCelularesReactivos] => a
            [slcMicroorganismos] => a
            [slcPresenciaCelulasEndometriales] => a
            [slcCelulasEscamosas] => a
            [slcCelulasGlandulares] => a
            [txtEspecificaciones] =>
            [txtIdMuestra] => 11

          idMuestraRespuesta
          idMuestra
          idUsuario
          tipoMuestra
          calidadMuestra
          categorizacionGeneral
          categorizacionGeneralEspecifique
          variacionesNoNeo
          cambiosCelularesReactivos
          microorganismos
          presenciaCelulasEndometriales
          celulasEscamosas
          celulasGlandulares
          especificaciones
          fechaHora

         */
        $data = array(
            'idMuestra' => $this->input->post('txtIdMuestra'),
            'idUsuario' => $this->session->userdata('us_id'),
            'tipoMuestra' => $this->input->post('slcTipoMuestra'),
            'calidadMuestra' => $this->input->post('slcCalidadMuestra'),
            'categorizacionGeneral' => $this->input->post('slcCategorizacionGeneral'),
            'categorizacionGeneralEspecifique' => $this->input->post('slcCategorizacionGeneralEspecifique'),
            'variacionesNoNeo' => $this->input->post('slcVariacionesNoNeo'),
            'cambiosCelularesReactivos' => $this->input->post('slcCambiosCelularesReactivos'),
            'microorganismos' => $this->input->post('slcMicroorganismos'),
            'presenciaCelulasEndometriales' => $this->input->post('slcPresenciaCelulasEndometriales'),
            'celulasEscamosas' => $this->input->post('slcCelulasEscamosas'),
            'celulasGlandulares' => $this->input->post('slcCelulasGlandulares'),
            'especificaciones' => $this->input->post('txtEspecificaciones'),
            'fechaHora' => ahora()
        );

        $rta = $this->Ciadmin_model->insertar_registro('hc_muestrarespuesta', $data);
        if ($rta) {

            $estado = 'D';

            if($this->input->post('slcCategorizacionGeneral') == 'b') $estado = 'X';

            $datamuestra = array(
                'estado' => $estado
            );

            $rtaM = $this->Ciadmin_model->update_by_id('hc_muestra', $datamuestra, 'idMuestra', $this->input->post('txtIdMuestra'));
            if ($rtaM) {
                redirect(base_url() . 'citotecnologo/muestra');
            } else {
                test('ERROR : No se pudo actualizar el estado de la Muestra');
            }

        } else {
            test('ERROR : No se pudo agregar diagnostico a la muestra');
        }


    }


    /*
     * -----------------------------------------------------------------------------
     *  Funciones Globales 
     * -----------------------------------------------------------------------------
     */

    /**
     * Función que imprime en la plantilla el resultante del controller
     * @param null $output
     */
    private function template_output($output = null)
    {
        $rolActual = $this->uri->segment(1);

        if (is_array($output)) {
            $output['rol_actual'] = $rolActual;
        } else {
            $output->rol_actual = $rolActual;
        }

        $this->load->view('template/template.php', $output);
    }

    /**
     * Encripta el valor de texto en un esquema de MD5
     * @param $post_array
     * @param null $primary_key
     * @return mixed
     */
    function encrypt_password_callback($post_array, $primary_key = null)
    {
        $this->load->helper('security');
        $post_array['contrasena'] = do_hash($post_array['contrasena'], 'md5');
        return $post_array;
    }

    /**
     * Imprime icono para el datatable enable/disable
     * @param $value
     * @param $row
     * @return string
     */
    function callback_indhabilitado($value, $row)
    {
        $icon = 'fa-check-square';
        $class = 'text-success';
        if ($value == '0') {
            $icon = 'fa-square-o';
            $class = '';
        }
        return '<div class="text-hide">' . $value . '</div><div class="text-center ' . $class . '"> <i class="fa fa-lg ' . $icon . '"></i> </div>';
    }

    /**
     * Genera el valor en un badge
     * @param $value
     * @param $row
     * @return string
     */
    function callback_badge($value, $row)
    {
        return '<div class="text-center"><span class="badge">' . $value . '</span></div>';
    }

    /**
     * Concatena el nombre en un solo string
     * @param $value
     * @param $row
     * @return string
     */
    function callback_fullname($value, $row)
    {
        $fullname = '';
        if ($row->primerNombre <> '') $fullname .= $row->primerNombre;
        if ($row->segundoNombre <> '') $fullname = $fullname . ' ' . $row->segundoNombre;
        if ($row->primerApellido <> '') $fullname = $fullname . ' ' . $row->primerApellido;
        if ($row->segundoApellido <> '') $fullname = $fullname . ' ' . $row->segundoApellido;

        if ($row->fechaNacimiento <> NULL) {
            $anios = calcularAnios($row->fechaNacimiento);
            $fullname = $fullname . '<span class="badge" style="float: right">' . $anios . ' años</span>';
        }

        return ucwords(strtolower($fullname));
    }



}

// END Citotecnologp class

/* End of file citotecnologp.php */
/* Location: ./application/controllers/citotecnologp.php */