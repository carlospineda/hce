<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('is_logged_in') || $this->session->userdata('rol_codigo') != 'A') {
            redirect('logout');
        }
    }

    public function index()
    {
        redirect($this->session->userdata('rol_url'));
    }

    public function dashboard()
    {
        $output = '';
        $datas['output'] = $this->load->view('admin/dashboard', $output, TRUE);
        $this->template_output($datas);
    }


    public function usuarios()
    {
        $crud = new grocery_CRUD();
        $crud->unset_jquery();

        $crud->set_table('ci_usuario')
            ->set_subject('Usuario')
            ->columns('nombre', 'apellido', 'idRol', 'email')
            ->required_fields('email', 'contrasena', 'nombre', 'apellido')
            ->set_field_upload('urlAvatar', 'assets/uploads/avatar')
            ->field_type('tipo', 'dropdown', array('S' => 'SuperAdministrador', 'A' => 'Administrador', 'I' => 'Ingeniero', 'M' => 'Maestro'))
            ->field_type('tipoDocumento', 'dropdown', array('CC' => 'Cedula de Ciudadania', 'NI' => 'NIT', 'CE' => 'Cedula de Extrangeria'))
            ->set_relation('idRol', 'ci_rol', 'nombre', array('indHabilitado' => 1))
            ->change_field_type('contrasena', 'password')
            ->callback_before_insert(array($this, 'encrypt_password_callback'))
            ->callback_before_update(array($this, 'encrypt_password_callback'));

        $output = $crud->render();
        $this->template_output($output);
    }




    /*
     * -----------------------------------------------------------------------------
     *  Funciones Globales
     * -----------------------------------------------------------------------------
     */

    function template_output($output = null)
    {
        $rolActual = $this->uri->segment(1);

        if (is_array($output)) {
            $output['rol_actual'] = $rolActual;
        } else {
            $output->rol_actual = $rolActual;
        }

        $this->load->view('template/template.php', $output);
    }

    function encrypt_password_callback($post_array, $primary_key = null)
    {
        $this->load->helper('security');

        if (preg_match('/^[a-f0-9]{32}$/', $post_array['contrasena']) == FALSE) {
            $post_array['contrasena'] = do_hash($post_array['contrasena'], 'md5');
            return $post_array;
        } else {
            return $post_array['contrasena'];
        }
    }

    function geolocalizacion($value = '', $primary_key = null)
    {
        $n = '<input id="field-latitud" type="text" name="latitud" value="' . $value . '" maxlength="20"> <a class="iframe btn btn-default" href="' . site_url("admin/geo") . '"> Geolocalizar </a>';
        return $n;
    }

    function callback_indhabilitado($value, $row)
    {
        $icon = 'fa-check-square';
        $class = 'text-success';
        if ($value == '0') {
            $icon = 'fa-square-o';
            $class = '';
        }
        return '<div class="text-hide">' . $value . '</div><div class="text-center ' . $class . '"> <i class="fa fa-lg ' . $icon . '"></i> </div>';
    }

}