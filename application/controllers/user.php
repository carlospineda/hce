<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Comprueba si el usuario ha iniciado sesión, si no,
     * lo manda al login
     * @return void
     */
    function index()
    {

    }

    /**
     * Compruebe el nombre de usuario y la contraseña con la base de datos
     * @return void
     */
    function validate_credentials()
    {

        $this->load->model('Users_model');
        $user_name = $this->input->post('user_name');
        $password = $this->__encrip_password($this->input->post('password'));

        $is_valid = NULL;
        $is_valid = $this->Users_model->validate($user_name, $password);

        if ($is_valid) {

            // Seleccionar el Avatar : AvatarBD - Gravatar - ImgPredeterminada
            $imgDefault = base_url() . 'assets/uploads/avatar/avatar.png';
            if ($is_valid->urlAvatar == '') {
                $urlAvatar = "http://www.gravatar.com/avatar/" . md5(strtolower(trim($is_valid->email))) . "?d=" . urlencode($imgDefault) . "&s=100";
            } else {
                $urlAvatar = base_url() . 'assets/uploads/avatar/' . $is_valid->urlAvatar;
            }

            $rolUsuario = $this->Ciadmin_model->select_by_id('ci_rol', 'idRol', $is_valid->idRol);

            $data = array(
                'us_id' => $is_valid->idUsuario,
                'us_uuid' => $is_valid->uuid,
                'us_nombre' => $is_valid->nombre,
                'us_apellido' => $is_valid->apellido,
                'us_avatar' => $urlAvatar,
                'us_email' => $is_valid->email,
                'rol_codigo' => $rolUsuario->codigo,
                'rol_url' => $rolUsuario->urlInicio,
                'rol_id' => $rolUsuario->idRol,
                'is_logged_in' => true
            );

            $this->session->set_userdata($data);

            // Actualizar campo ultimoAcceso en la tabla usuario
            $datosUpdate = array(
                'ultimoAcceso' => date('Y-m-d H:i:s')
            );
            $this->Users_model->actualizar_usuario($is_valid->idUsuario, $datosUpdate);

            // Mensaje de Bienvenida
            ca_growl('Bienvenido', 'Hola de nuevo <strong>' . $is_valid->nombre . '</strong>');
            log_message('info', 'Acceso Usuario - ' . $is_valid->idUsuario . ' - ' . $is_valid->nombre . ' ' . $is_valid->apellido);

            $this->__redirectLogIn();


        } else {
            $data['message_error'] = TRUE;
            $this->signin($data);
        }
    }

    /**
     * Encriptar la contraseña en MD5
     * @return mixed
     */
    function __encrip_password($password)
    {
        return md5($password);
    }

    function __redirectLogIn()
    {
        if ($this->session->userdata('is_logged_in')) {
            redirect(base_url() . $this->session->userdata('rol_url'));
        } else {
            $this->load->view('login');
        }
    }

    /**
     * Metdo que carga el Login
     * @return void
     */
    function signin($data = '')
    {
        $this->load->view('login', $data);
    }

    /**
     * Crear nuevo usuario y almacenarlo en la base de datos
     * @return void
     */
    function create_member()
    {
        $this->load->library('form_validation');

        // field name, error message, validation rules
        $this->form_validation->set_rules('first_name', 'Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('signup_form');
        } else {
            $this->load->model('Users_model');

            if ($query = $this->Users_model->create_member()) {
                $this->load->view('signup_successful');
            } else {
                $this->load->view('signup_form');
            }
        }
    }

    /**
     * Extrae la info del usuario y la muestra en el view
     */
    function perfil_usuario()
    {
        $this->load->model('Users_model');
        $usuario_id = $this->session->userdata('us_id');
        $data = $this->Users_model->get_perfil_usuario($usuario_id);
        //print_r($data);
        $this->load->view('template/usuario_perfil', $data);
    }

    /**
     * Muestra el View de cambio de contraseña
     */
    function cambiarcontrasena_usuario()
    {
        $this->load->view('template/usuario_cambiarcontrasena');
    }

    /**
     * Proceso de cambio de contraseña
     * http://stackoverflow.com/questions/14545104/codeigniter-change-password
     *
     */
    function cambiarcontrasenaproceso_usuario()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('password1', '', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', '', 'trim|required|min_length[4]|max_length[32]|matches[password1]');

        if ($this->form_validation->run() == FALSE) {

            // Mensaje
            ca_growl('Cambio de Contraseña', 'Ocurrio un error y NO se cambio su contraseña', 4);
            redirect($_SERVER['HTTP_REFERER']);

            //$this->change_password();
        } else {
            $this->load->model('Users_model');

            $contrasena = $this->__encrip_password($this->input->post('password1'));

            //Actualizar contraseña
            $datosUpdate = array(
                'contrasena' => $contrasena
            );
            $rta = $this->Users_model->actualizar_usuario($this->session->userdata('us_id'), $datosUpdate);
            if ($rta == 1) {
                // Mensaje de Confirmación
                ca_growl('Cambio de Contraseña', 'El cambio realizado con exito!', 2);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                ca_growl('Cambio de Contraseña', 'Ocurrio un error y NO se cambio su contraseña<br />Intentelo nuevamente', 2);
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    /**
     * Muestra View para editar el perfil del usuario
     */
    function editarperfil_usuario()
    {
        $this->load->model('Users_model');
        $usuario_id = $this->session->userdata('us_id');
        $data = $this->Users_model->get_perfil_usuario($usuario_id);

        $this->load->view('template/usuario_editarperfil', $data);
    }

    /**
     * proceso del actualizacion del perfil del usuario actual
     */
    function editarperfilproceso_usuario()
    {

        $this->load->model('Users_model');

        /* FALTA :  Validacion formulario */

        $datosUpdate = array(
            'tipoDocumento' => $this->input->post('slcDocumento'),
            'numDocumento' => $this->input->post('txtNumeroDocumento'),
            'nombre' => $this->input->post('txtNombre'),
            'apellido' => $this->input->post('txtApellido'),
            'direccion' => $this->input->post('txtDireccion'),
            'telefono' => $this->input->post('txtTelefono'),
            'celular' => $this->input->post('txtCelular'),
        );

        $rta = $this->Users_model->actualizar_usuario($this->session->userdata('us_id'), $datosUpdate);

        if ($rta == 1) {
            // Mensaje de Confirmación
            ca_growl('Perfil', 'La actualización de sus datos se ha realizado con exito!', 2);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            ca_growl('Error', 'Ocurrio un error actualizando los datos<br />Intentelo nuevamente', 4);
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * Muestra el view para cambiar la imagen del perfil del usuario actual
     * http://www.jeswin.com/professional/programming/codeigniter-uploading-an-image
     */
    function uploadimagenperfil_usuario()
    {
        $this->load->view('template/usuario_editaravatar');
    }

    /**
     * Proceso cambiar la imagen del perfil del usuario actual
     */
    function uploadimagenperfilproceso_usuario()
    {
        if ($_FILES['avatar']['error'] == 0) {

            //upload and update the file
            $config['upload_path'] = './assets/uploads/avatar/'; // Directorio de los avatar
            $config['allowed_types'] = 'gif|jpg|png';
            $config['overwrite'] = TRUE;
            $config['remove_spaces'] = TRUE;
            //$config['max_size']   = '100';// en KB

            $this->load->library('upload', $config); //codeigniter default function

            if (!$this->upload->do_upload('avatar')) {
                $upload_errors = $this->upload->display_errors('', '');

                // Mensaje de Error
                ca_growl('Error', 'Ocurrio un error al tratar de cambiar su Avatar <br />' . $upload_errors, 4);
                redirect($_SERVER['HTTP_REFERER']);
            } else {

                // Cambiar tamaño de la imagen
                $config['source_image'] = $this->upload->upload_path . $this->upload->file_name;
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 100; // image re-size  properties
                $config['height'] = 100; // image re-size  properties

                $this->load->library('image_lib', $config); //codeigniter default function

                if (!$this->image_lib->resize()) {
                    // Mensaje de Error
                    ca_growl('Error', 'Ocurrio un error al tratar de cambiar su Avatar <br />' . $upload_errors, 4);
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $this->load->model('Users_model');
                $nuevoAvatar = $this->Users_model->update_profile_pic($this->session->userdata('us_id'));

                // Actualización dato session (urlAvatar)
                $this->session->set_userdata('us_avatar', base_url() . 'assets/uploads/avatar/' . $nuevoAvatar);

                ca_growl('Cambio de Avatar', 'Su imagen de perfil fue cambiado correctamente!', 2);
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {

            ca_growl('Error', 'Ocurrio un error al tratar de cambiar su Avatar <br />' . $upload_errors, 4);
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * Destruye la session de Usuario
     * @return void
     */
    function logout()
    {
        log_message('info', 'LogOut Usuario - ' . $this->session->userdata('us_id') . ' - ' . $this->session->userdata('us_nombre') . ' ' . $this->session->userdata('us_apellido'));

        // Creamos el array de la sesion con valores vacios
        $data = array(
            'us_id' => '',
            'us_uuid' => '',
            'us_nombre' => '',
            'us_apellido' => '',
            'us_rol' => '',
            'us_avatar' => '',
            'us_email' => '',
            'is_logged_in' => '',
            'rol_url' => '',
            'rol_id' => ''
        );

        $this->session->unset_userdata($data);
        unset($this->session->userdata);
        $this->session->sess_destroy();
        redirect('login');
    }

    /**
     * Cambio de Contraseña
     * @return Boolean (true : success, false : fail)
     */
    function cambiocontrasena()
    {
        $this->load->model('Users_model');

        // Get post -> user
        $user = $this->input->post('user');

        // validar si existe usuario
        $where_array = array(
            'email' => $user
        );

        $existe = $this->Ciadmin_model->total_registros('ci_usuario', $where_array);

        if ($existe == 1) {
            // Generar nueva contraseña
            $nuevoPass = $this->random_password();
            echo $nuevoPass;

            // cambiar contraseña actual

            // enviar correo de cambio de contraseña
        } else {
            echo false;
        }

    }

    /**
     * Generar contraseña randomica
     *
     * @return String Nueva Contraseña
     */
    function random_password()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array();
        $alpha_length = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password);
    }
}