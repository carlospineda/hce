<?php

$config['version_ciadmin'] = '2.7';

$config['nombre_app'] = 'Patología Digital';
$config['subnombre_app'] = 'El paradigma diagnóstico más importante de la medicina';
$config['icon_app'] = 'fa-stethoscope';
$config['version_app'] = '1.0';

$config['id_app'] = 'CBRDX-HCE';
$config['ehrserver_on'] = false;