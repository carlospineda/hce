<?php

$config['fire_log_strip_tags'] = TRUE;
$config['fire_log_param_dilem'] = "::";
$config['fire_log_pagination_settings'] = array(
    'num_links' => 20,
    'full_tag_open' => '<div><ul class="pagination pagination-small pagination-centered">',
    'full_tag_close' => '</ul></div>',
    'num_tag_open' => '<li>',
    'num_tag_close' => '</li>',
    'cur_tag_open' => '<li class="active"><a>',
    'cur_tag_close' => '</a></li>',
    'next_tag_open' => '<li>',
    'next_tag_close' => '</li>',
    'prev_tag_open' => '<li>',
    'prev_tag_close' => '</li>'
);