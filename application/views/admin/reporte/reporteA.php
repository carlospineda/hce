<div id="reporte">

    <div class="row">
        <div class="col-md-12">

            <div class="error" style="text-align: center; margin-top: 60px; margin-bottom: 100px;">
                <div style="text-align: center;">
                    <i class="fa fa-line-chart fa-5x"></i>
                </div>
                <h2>El reporte no esta configurado o no fue encontrado</h2>

                <div class="error-desc">

                    Lo sentimos, pero la página que buscas no fue encontrada o no existe. <br/>
                    Intente actualizar la página o haga clic en el botón de abajo para volver a la página de inicio.
                    <div>
                        <a class=" login-detail-panel-button btn" href="dashboard">
                            <i class="fa fa-arrow-left"></i>
                            Volver al Dashboard                        
                        </a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>