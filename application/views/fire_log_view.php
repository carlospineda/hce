<div id="logView">
    <div class="panel panel-default grid">
        <div class="panel-heading">
            <i class="fa fa-terminal"></i>
            <?php echo $log_file_name ?><?php if ($today) echo ' - ' . $this->lang->line('fire_log_today'); ?>
            <div class="panel-tools">
                <div class="btn-group" style="float: right;">
                    <a href="<?php echo build_spark_url(array('delete' => $log_file_name), TRUE) ?>"
                       onclick="return confirm('¿Estás seguro?');" class="btn">
                        <i class="fa fa-times-circle"></i> Eliminar Archivo Actual </a>
                    <a href="<?php echo build_spark_url(array('delete_all' => $log_file_name), TRUE) ?>"
                       onclick="return confirm('¿Estás seguro?');" class="btn">
                        <i class="fa fa-times-circle"></i> Eliminar Todos </a>
                </div>
            </div>
        </div>
        <div class="panel-body filters">
            <div class="row">
                <div class="col-md-9">

                    <div class="btn-group">
                        <?php
                        echo build_filter_link('all', 'MOSTRAR TODO');
                        echo build_filter_link('error', 'ERRORES');
                        echo build_filter_link('info', 'INFO');
                        echo build_filter_link('debug', 'DEBUG');
                        ?>
                    </div>

                </div>

            </div>
        </div>

        <div class="logViewcontainer">
            <div id="logViewnav">
                <div class="btn-group-vertical">
                    <?php
                    foreach ($list as $file) {
                        echo build_log_link($file, $log_file_name);
                    }
                    ?>
                </div>
            </div>
            <div class="logViewcontainer">
                <div>
                    <?php echo str_replace("&nbsp;", '', $pagination_links); ?>
                </div>

                <div class="logViewlogContainer">
                    <?php echo $log_contents ?>
                </div>

                <div>
                    <?php echo str_replace("&nbsp;", '', $pagination_links); ?>
                </div>

            </div>
        </div>
    </div>


    <div class="panel-footer">
        <i class="fa fa-terminal"></i>
        <?php echo $log_file_name ?><?php if ($today) echo ' - ' . $this->lang->line('fire_log_today'); ?>
    </div>
</div>

</div>
