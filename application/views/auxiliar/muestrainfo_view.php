<!--
    [primerNombre] => Paciente
    [segundoNombre] =>
    [primerApellido] => Pruebs
    [segundoApellido] =>
    [codTipoDocumento] => CC
    [tipoDocumento] => Cedula
    [numeroDocumento] => 555554
    [fechaNacimiento] => 1989-03-11
    [fechaHora] => 2019-04-25 22:45:45
    [estado] => A
    [nombre] => Auxiliar
    [apellido] => Patologia
-->
<?php

$paciente = $primerNombre . ' ' . $segundoNombre . ' ' . $primerApellido . ' ' . $segundoApellido;
$documento_codigo = 'CC';
$documento_numero = '123454321';
$muestra_fecha = ahora();
?>

<div class="panel panel-default">
    <div class="panel-body clearfix"
         style="position: relative;position: relative;padding: 1px 15px;background-color: #f9f9f9;">
        <i class="fa fa-user fa-4x" aria-hidden="true" style="float: left;margin: 14px;"></i>
        <h4 style="margin-bottom: 0px;"><?php echo $paciente; ?> <span class="badge"><? echo calcularAnios($fechaNacimiento) ?> años</span></h4>
        <h5 style="margin: 1px;"><?php echo $codTipoDocumento . ' ' . $numeroDocumento ?></h5>
        <?php echo $muestra_fecha; ?><br>
    </div>
</div>