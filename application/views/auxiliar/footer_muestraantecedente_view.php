<script type="text/javascript">

    $(function () {

        $('input[type=radio][name=indActivoSexual]').change(function () {
            if (this.value == 1){
                $('#fechaUltimaRelacionSexual_field_box').show();
            } else {
                $('#fechaUltimaRelacionSexual_field_box').hide();
            }
        });


        // indCitologiaPrevia
        $('#fechaCitologiaPrevia_field_box, #resultadoCitologiaPrevia_field_box, #descripcionCitologiaPrevia_field_box').hide();
        $('input[type=radio][name=indCitologiaPrevia]').change(function () {
            if (this.value == 1){
                $('#fechaCitologiaPrevia_field_box, #resultadoCitologiaPrevia_field_box, #descripcionCitologiaPrevia_field_box').show();
            } else {
                $('#fechaCitologiaPrevia_field_box, #resultadoCitologiaPrevia_field_box, #descripcionCitologiaPrevia_field_box').hide();
            }
        });

        // indCitologiaPrevia
        $('#metodoPlanificacion_field_box').hide();
        $('input[type=radio][name=indPanifica]').change(function () {
            if (this.value == 1) {
                $('#metodoPlanificacion_field_box').show();
            } else {
                $('#metodoPlanificacion_field_box').hide();
            }
        });


        //Inicio
        if ($('input[type=radio][name=indActivoSexual]').val() == 1)
            $('#fechaUltimaRelacionSexual_field_box').show();

        if ($('input[type=radio][name=indCitologiaPrevia]').val() == 1)
            $('#fechaCitologiaPrevia_field_box, #resultadoCitologiaPrevia_field_box, #descripcionCitologiaPrevia_field_box').show();

        if ($('input[type=radio][name=indPanifica]').val() == 1)
            $('#metodoPlanificacion_field_box').show();


    });

</script>