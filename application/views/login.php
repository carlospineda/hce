<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge; FF=3; chrome=1; OtherUA=4"/>

    <title>Inicio de sesión | <?php echo $this->config->item('nombre_app'); ?> </title>

    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Last-Modified" content="0"/>

    <meta name="robots" content="noindex,nofollow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="">
    <meta name="author" content="CarlosAPinedaT - Ciberdix">

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/application.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/login.css">

</head>
<body>
<?php
if (isset($message_error) && $message_error) {
    echo '<div class="alert alert-warning alert-dismissible text-center" role="alert">';
    echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>';
    echo '<strong>Opsss!</strong> Usuario y/o Contraseña están incorrectas. Intentelo nuevamente!</div>';
    echo '</div>';
}
?>

<div id="login_box">
    <?php
    $attributes = array('class' => 'form-horizontal');
    echo form_open('login/validate_credentials', $attributes);
    // echo '<div style="text-align: center"><img src="' . base_url() . 'assets/images/directrix_logo.png" alt="Directrix" style="margin: 0px auto 30px; height: 64px;"/></div>';
    echo '<h1 style="text-align: center;margin-bottom: 1px;"><i class="fa ' . $this->config->item('icon_app') . '"></i> ' . $this->config->item('nombre_app') . '</h1>';
    echo '<small style="margin-bottom: 4px;">'.$this->config->item('subnombre_app') .'</small>';
    echo '<div class="input-group input-block-level" style="margin-top: 23px;"><span class="input-group-addon"> <i class="fa fa-envelope"></i> </span>';
    echo form_input('user_name', '', 'placeholder="Correo Electrónico" class="form-control"');
    echo '</div>';

    echo '<div class="input-group input-block-level"><span class="input-group-addon"> <i class="fa fa-lock"></i> </span>';
    echo form_password('password', '', 'placeholder="Contraseña" class="form-control"');
    echo '</div>';

    echo form_submit('submit', 'Iniciar Sesión', 'class="btn btn-block btn-primary"');

    echo '<div id="recordarcontrasena"><a href="#">Recordar Contraseña?</a></div> ';

    echo '<div id="version_app">v'.$this->config->item('version_app').'</div>';

    echo form_close();
    ?>
</div>


<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Recordar Contraseña</h4>
            </div>
            <div class="modal-body">
                Ingrese su usuario y de click en el boton "Generar Nueva Contraseña" y una nueva contraseña se
                enviará al correo asociado a su cuenta.
                <form class="form-horizontal">
                    <div class="input-group input-block-level" style="width: 60%;margin: 0 auto;padding: 20px 10px 0;">
                        <span class="input-group-addon"> <i class="fa fa-user"></i> </span>
                        <input type="text" id="user_name_recovery" name="user_name_recovery" value=""
                               placeholder="usuario" class="form-control">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Cerrar</a>
                <a href="#" id="btnRecovery" class="btn btn-primary">Generar Nueva Contraseña</a>
            </div>
        </div>
    </div>
</div>


<!-- Javascripts -->
<script src="<?php echo base_url(); ?>assets/javascripts/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/javascripts/jquery.plugins.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/application.js"></script>

<script>

    /* Generar random de la imagen de fondo */
    const min = 1;
    const max = 3;
    const random = Math.floor(Math.random() * (max - min + 1)) + min;
    jQuery('body').css('background-image', 'url("assets/images/bg/bg_login_' + random + '.jpg")');


    jQuery(function () {
        jQuery('#login_box').center();
        jQuery('#txtUsuario').focus();
        jQuery(window).resize(function () {
            jQuery('#login_box').center();
        });

        // Olvido contraseña
        jQuery('#recordarcontrasena').on('click', function () {
            jQuery('#myModal').modal({show: true})
            jQuery('#user_name_recovery').val('').focus();
        });

        // Envio de contraseña
        jQuery('#btnRecovery').on('click', function () {

            user = jQuery('#user_name_recovery').val();

            if (user != '') {
                $.post("recovery", {
                    user: jQuery('#user_name_recovery').val()
                }).done(function (data) {
                    jQuery('#user_name_recovery').val('');
                    if (data == true) {
                        alert('La nueva contraseña fue enviada al correo!');
                        jQuery('#myModal').modal({show: false})
                    } else {
                        alert('ocurrio un error al tratar de reestablecer la contraseña\nVuelve a intentarlo o contacte al administrador del sistema!');
                    }

                });
            } else {
                alert('Debe escribir un usuario para recuperar la contraseña');
            }

        });
    });
</script>

</body>
</html>