<ul id="dock">
    <li class="launcher<?php if ($this->uri->segment(2) == 'dashboard') echo ' active'; ?>">
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/dashboard">Inicio</a>
    </li>

    <li class="launcher<?php if ($this->uri->segment(2) == 'paginas') echo ' active'; ?>">
        <i class="fa fa-file-text-o"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/contenido">Páginas</a>
    </li>
</ul>