<div>
    <div>
        <div class="col-sm-12" >
            <div class="panel panel-default" style="margin-top: 10px;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo $nombre . ' ' . $apellido ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"> 
                            <img alt="Avatar Usuario" src="<?php echo $this->session->userdata('us_avatar') ?>" class="img-thumbnail"> 
                        </div>
                        <div class=" col-md-9 col-lg-9 "> 
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td>Correo Electronico :</td>
                                        <td><?php echo $email ?></td>
                                    </tr>
                                    <tr>
                                        <td>Dirección :</td>
                                        <td><?php echo $direccion ?></td>
                                    </tr>
                                    <tr>
                                        <td>Telefono :</td>
                                        <td><?php echo $telefono ?></td>
                                    </tr>
                                    <tr>
                                        <td>Celular :</td>
                                        <td><?php echo $celular ?></td>
                                    </tr>
                                    <tr>
                                        <td>Usuario desde :</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Último Acceso :</td>
                                        <td><?php echo $ultimoAcceso ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <a href="<?php echo base_url(); ?>user/uploadimagenperfil_usuario" class="fancybox btn btn-info btn-sm" data-fancybox-type="ajax" data-fancybox-title="Cambiar Avatar">
                                <i class="fa fa-picture-o"></i> Cambiar Avatar
                            </a>
                            <a href="<?php echo base_url(); ?>user/cambiarcontrasena_usuario" class="fancybox btn btn-info btn-sm" data-fancybox-type="ajax" data-fancybox-title="Cambiar Contraseña">
                                <i class="fa fa-unlock-alt"></i> Cambiar Contraseña
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right;">  

                    <a href="<?php echo base_url(); ?>user/editarperfil_usuario" class="btn btn-primary fancybox" data-fancybox-type="ajax" data-fancybox-title="Editar Perfil"> 
                        <i class="fa fa-pencil-square-o"></i> Editar Perfil 
                    </a>

                </div>

            </div>
        </div>
    </div>
</div>