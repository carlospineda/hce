<div>
    <div>
        <div class="col-sm-12" >

            <div class="panel panel-info" style="margin-top: 10px;">
                <?php
                $atributos = array('class' => 'form-horizontal', 'id' => 'frmEditarUsuario');
                echo form_open('user/editarperfilproceso_usuario', $atributos);
                ?>
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php echo $nombre . ' ' . $apellido ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-3 col-lg-3 " align="center"> 
                            <img alt="Avatar Usuario" src="<?php echo $this->session->userdata('us_avatar') ?>" class="img-thumbnail" />                           
                        </div>
                        <div class=" col-md-9 col-lg-9 ">

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtEmail">Correo Electronico :</label>  
                                    <div class="col-md-8">
                                        <input id="txtEmail" name="txtEmail" type="text" class="form-control" value="<?php echo $email ?>" readonly />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtNombre">Nombre :</label>  
                                    <div class="col-md-8">
                                        <input id="txtNombre" name="txtNombre" type="text" class="form-control" value="<?php echo $nombre ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtApellido">Apellido :</label>  
                                    <div class="col-md-8">
                                        <input id="txtApellido" name="txtApellido" type="text" class="form-control" value="<?php echo $apellido ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="slcDocumento">Tipo de Documento :</label>  
                                    <div class="col-md-8">
                                        <select id="slcDocumento" name="slcDocumento" class="form-control">
                                            <option> - Escoja -</option>
                                            <option value="CC" <?php if ($tipoDocumento == 'CC') echo 'selected' ?> >Cedula de Ciudadania</option>
                                            <option value="NI" <?php if ($tipoDocumento == 'NI') echo 'selected' ?> >NIT</option>
                                            <option value="CE" <?php if ($tipoDocumento == 'CE') echo 'selected' ?> >Cedula de Extrangeria</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtNumeroDocumento">Número de Documento :</label>  
                                    <div class="col-md-8">
                                        <input id="txtDocumento" name="txtNumeroDocumento" type="text" class="form-control" value="<?php echo $numDocumento ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtDireccion">Dirección :</label>  
                                    <div class="col-md-8">
                                        <input id="txtDireccion" name="txtDireccion" type="text" class="form-control" value="<?php echo $direccion ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtTelefono">Telefono :</label>  
                                    <div class="col-md-8">
                                        <input id="txtTelefono" name="txtTelefono" type="text" class="form-control" value="<?php echo $telefono ?>" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="txtCelular">Celular :</label>  
                                    <div class="col-md-8">
                                        <input id="txtCelular" name="txtCelular" type="text" class="form-control" value="<?php echo $celular ?>" />
                                    </div>
                                </div>
                            </fieldset>
                            
                            <p>
                                <a href="<?php echo base_url(); ?>user/uploadimagenperfil_usuario" class="fancybox btn btn-info btn-sm" data-fancybox-type="ajax" data-fancybox-title="Cambiar Avatar"> 
                                    <i class="fa fa-picture-o"></i> Cambiar Avatar
                                </a>
                                <a href="<?php echo base_url(); ?>user/cambiarcontrasena_usuario" class="fancybox btn btn-info btn-sm" data-fancybox-type="ajax" data-fancybox-title="Cambiar Contraseña">
                                    <i class="fa fa-unlock-alt"></i> Cambiar Contraseña
                                </a>
                            </p>

                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right;">  
                    <?php
                    $data = array(
                        'name'      => 'btnEditarPerfil',
                        'id'        => 'btnEditarPerfil',
                        'content'   => '<i class="fa fa-pencil-square-o"></i> Guardar Perfil',
                        'type'      => 'submit',
                        'class'     => 'btn btn-primary'
                    );

                    echo form_button($data);
                    ?>
                </div>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>