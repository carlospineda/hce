<ul id="dock">
    <li class="launcher<?php if ($this->uri->segment(2) == 'dashboard') echo ' active'; ?>">
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/dashboard">Inicio</a>
    </li>

    <li class="launcher<?php if ($this->uri->segment(2) == 'usuarios') echo ' active'; ?>">
        <i class="fa fa-group"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/usuarios">Usuarios</a>
    </li>

    <li class="launcher dropdown hover">
        <i class="fa fa-sliders"></i>
        <a href="#">Configuración</a>
        <ul class="dropdown-menu">
            <li class="dropdown-header">Configuración</li>
            <li><a href="...">...</a></li>
        </ul>
    </li>
</ul>