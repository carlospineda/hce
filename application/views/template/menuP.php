<ul id="dock">
    <li class="launcher<?php if ($this->uri->segment(2) == 'dashboard') echo ' active'; ?>">
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/dashboard">Inicio</a>
    </li>
    <li class="launcher<?php if ($this->uri->segment(2) == 'muestra') echo ' active'; ?>">
        <i class="fa fa-window-maximize"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/muestra">Citologia</a>
    </li>
    <li class="launcher<?php if ($this->uri->segment(2) == 'pacientes') echo ' active'; ?>">
        <i class="fa fa-users"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/pacientes">Pacientes</a>
    </li>
</ul>