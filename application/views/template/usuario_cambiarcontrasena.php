<div style="width: 400px;">
    <div class="col-sm-12">
        <p class="text-center">
            <br />Utilice el siguiente formulario para cambiar tu contraseña. La contraseña no puede ser el mismo que su nombre de usuario.<br />
        </p>
        <?php
        echo form_open('user/cambiarcontrasenaproceso_usuario');
        ?>
            <?php
            echo form_password('password1', set_value(''), ' class="form-control" placeholder="Nueva Contraseña" autocomplete="off" ');
            ?>
            <br />
            <?php
            echo form_password('password2', set_value(''), ' class="form-control" placeholder="Repetir Contraseña" autocomplete="off" ');
            ?>
            <br />
            <?php
            echo form_submit('submit', 'Cambiar Contraseña', 'class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Cambiando Contraseña..." ');
            ?>
            <br />
        <?php
        echo form_close();
        ?>
    </div>
</div>