<div class="col-sm-12" >
    <div class="panel panel-default" style="margin-top: 10px;">
        <?php
        $atributos = array('class' => 'form-horizontal');
        echo form_open_multipart('user/uploadimagenperfilproceso_usuario', $atributos);
        ?>
        <div class="panel-heading">
            <h3 class="panel-title">
                <?php echo $this->session->userdata('us_nombre') ?> <?php echo $this->session->userdata('us_apellido') ?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">

                <div class="col-md-4" align="center"> 
                    <img src="<?php echo $this->session->userdata('us_avatar') ?>" alt="Avatar Usuario" class="img-thumbnail" />                           
                </div>
                <div class=" col-md-8">

                    <fieldset>
                        <div class="form-group">
                            <label class="control-label" for="avatar">Selecionar Imagen</label>  
                            <?php
                            $Fdata = array(
                                'name'  => 'avatar', 
                                'class' => 'file form-control'
                                ); 
                            echo form_upload($Fdata); 
                            ?>
                        </div>
                    </fieldset>

                </div>
            </div>

        </div>
        <div class="panel-footer" style="text-align: right;">
            <?php
            $data = array(
                'name'      => 'btnEditarPerfil',
                'id'        => 'btnEditarPerfil',
                'content'   => '<i class="fa fa-pencil-square-o"></i> Guardar Nuevo Avatar',
                'type'      => 'submit',
                'class'     => 'btn btn-primary'
            );

            echo form_button($data);
            ?>
        </div>
        <?php
        echo form_close();
        ?>
    </div>
</div>