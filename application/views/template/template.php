<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]>
<html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<html class="no-js" lang="es" class="loading">
<head>

    <meta content="text/html" charset="utf-8" http-equiv="content-type"/>
    <meta content="ie=edge,chrome=1" http-equiv="x-ua-compatible"/>
    <meta content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" name="viewport"/>
    <meta content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)" name="viewport"/>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="translucent-black" name="apple-mobile-web-app-status-bar-style"/>

    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="CarlosAPinedaT - Ciberdix"/>
    <meta name="description" content=""/>

    <title> <?php echo ucfirst($this->uri->segment(2)); ?> | <?php echo $this->config->item('nombre_app'); ?> </title>

    <link href="assets/images/favicon.ico" rel="shortcut icon"/>
    <link href="assets/images/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed"/>
    <link href="assets/images/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed" sizes="57x57"/>
    <link href="assets/images/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72"/>
    <link href="assets/images/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed"
          sizes="114x114"/>

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/application.css"
          media="screen"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>assets/javascripts/fancybox2.1.5/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans"/>


    <?php if (isset($css_files)) {
        foreach ($css_files as $file): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $file; ?>"/>
        <?php endforeach;
    } ?>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/jquery.jgrowl.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/main.css"/>

    <!-- Javascripts -->
    <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/javascripts/fancybox2.1.5/jquery.fancybox.pack.js"></script>
    <script src="<?php echo base_url(); ?>assets/javascripts/jquery.plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/modernizr.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/application.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>assets/javascripts/bootstrap-growl/bootstrap-growl.min.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>assets/javascripts/jquery.ckfinder/ckfinder.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/main.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


    <?php if (isset($js_files)) {
        foreach ($js_files as $file):
            ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach;
    } ?>

</head>
<body class="main page">

<div id="loading_layer" style="display:none">
    <img src="<?php echo base_url(); ?>assets/images/ajax_loader.gif" alt="cargando..." />
</div>

<!-- Navbar -->
<div class="navbar navbar-default" id="navbar">
    <a class="navbar-brand" href="#">
        <?php echo '<h2 style="margin-top: 10px;"><i class="fa ' . $this->config->item('icon_app') . '"></i> ' . $this->config->item('nombre_app') . '</h2>' ?>
    </a>
    <ul class="nav navbar-nav pull-right">
        <li class="hidden-xs hidden-sm">
            <a href="#"><i class="fa fa-clock-o"></i> <span id="hora-actual">4:32 PM</span> </a>
        </li>
        <li class="hidden-xs hidden-sm">
            <a href="#"><i class="fa fa-calendar"></i> <span id="fecha-actual">10/06/2014</span> </a>
        </li>
        <li class="dropdown user">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="thumb-small avatar inline">
                            <img class="img-thumbnail" alt="<?php echo $this->session->userdata('us_nombre') ?>"
                                 src="<?php echo $this->session->userdata('us_avatar') ?>"/>
                        </span>
                <strong> <?php echo $this->session->userdata('us_nombre') ?> </strong>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo base_url(); ?>user/perfil_usuario" class="fancybox" data-fancybox-type="ajax"
                       data-fancybox-title="Mi Perfil">
                        <i class="fa fa-wrench"></i> Mi Perfil
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>user/cambiarcontrasena_usuario" class="fancybox"
                       data-fancybox-type="ajax" data-fancybox-title="Cambiar Contraseña">
                        <i class="fa fa-unlock"></i> Cambiar Contraseña
                    </a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url(); ?>user/logout"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- FIN Navbar -->

<!-- Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <section id="sidebar">
        <i class="fa fa-align-justify fa fa-large" id="toggle"></i>
        <?php $this->load->view('template/menu' . $this->session->userdata('rol_codigo')); ?>
        <div id="beaker" data-toggle="tooltip" title="Powered Carlos Pineda"></div>
    </section>
    <!-- FIN Sidebar -->

    <!-- Tools -->
    <section id="tools">
        <ul class="breadcrumb" id="breadcrumb">
            <?php if (isset($breads)) { ?>
                <li class="title"> <?php echo $breads ?> </li>
            <?php } ?>
            <li class="title"> <?php echo ucfirst($this->uri->segment(2)); ?> </li>
        </ul>
    </section>
    <!-- FIN Tools -->

    <!-- Content -->
    <div id="content">
        <?php if (isset($output_top)) {
            echo $output_top . '<p></p>';
        } ?>

        <?php if (isset($output_top_b)) {
            echo $output_top_b . '<p></p>';
        } ?>

        <?php if (isset($output)) {
            echo $output;
        } ?>

        <?php if (isset($output_botton)) {
            echo '<p></p>' . $output_botton;
        } ?>
        <?php if (isset($output_botton_b)) {
            echo '<p></p>' . $output_botton_b;
        } ?>
        <br/>
        <?php if (isset($back_link)) { ?>
            <div class="pnl-back">
                <a href="javascript:history.back()" class="btn btn-primary btn-sm" style="margin: 10px 0px;">
                    <i class="fa fa-angle-left" aria-hidden="true"></i> ir atras
                </a>
            </div>
        <?php } ?>
        <div class="row copy">
            <div class="col-md-5">
                <?php echo date('Y') ?> &copy; <?php echo $this->config->item('nombre_app'); ?>
                v<?php echo $this->config->item('version_app'); ?> - Apache License 2.0<br>Powered by <a
                        href="https://github.com/CarlosPinedaT">carlospinedat</a>
            </div>
            <div class="col-md-offset-2 col-md-5 text-right hidden-xs hidden-sm">
                Página generada en <strong>{elapsed_time}</strong> segundos<br />Powered by CI v<?php echo CI_VERSION; ?>
                and CIAdmin v<?php echo $this->config->item('version_ciadmin'); ?><br />
                Memory Usage: <strong>{memory_usage}</strong>
            </div>
        </div>
    </div>
    <!-- FIN Content -->

</div>
<!-- FIN Wrapper -->

<?php if ($this->session->flashdata('msgMensaje') != ''): ?>
    <script type="text/javascript">
        <!--
        $(function () {
            $.growl({
                icon: 'fa <?php echo $this->session->flashdata('msgIcono') ?>',
                title: '<?php echo $this->session->flashdata('msgTitulo') ?>',
                message: '<?php echo $this->session->flashdata('msgMensaje') ?>'
            }, {
                template: {
                    title_divider: '<hr class="separator" />'
                },
                type: '<?php echo $this->session->flashdata('msgTipo'); ?>'
            });
        });
        //-->
    </script>
<?php endif; ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130121984-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-130121984-1');
    gtag('userId', <?php echo $this->session->userdata('us_id') ?>);
</script>

</body>
</html>