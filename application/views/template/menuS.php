<ul id="dock">
    <li class="launcher<?php if ($this->uri->segment(2) == 'dashboard') echo ' active'; ?>">
        <i class="fa fa-home"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/dashboard">Inicio</a>
    </li>

    <li class="launcher<?php if ($this->uri->segment(2) == 'pacientes') echo ' active'; ?>">
        <i class="fa fa-users"></i>
        <a href="<?php echo base_url() . $this->uri->segment(1); ?>/pacientes">Pacientes</a>
    </li>

    <li class="launcher dropdown hover">
        <i class="fa fa-server"></i>
        <a href="#">Sistema</a>
        <ul class="dropdown-menu">
            <li class="dropdown-header">Sistema</li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/roles">Roles</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/usuarios">Usuarios</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/pais">Pais</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/departamento">Departamento</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/ciudad">Ciudad</a></li>
        </ul>
    </li>

    <li class="launcher dropdown hover">
        <i class="fa fa-sliders"></i>
        <a href="#">Configuración</a>
        <ul class="dropdown-menu">
            <li class="dropdown-header">Configuración</li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/estadocivil">Estado Civil</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/factorrh">Factor RH</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/genero">Género</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/tipocontacto">Tipos de Contacto</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/tipodocumento">Tipos de documento</a></li>
        </ul>
    </li>

    <li class="launcher dropdown hover">
        <i class="fa fa-terminal"></i>
        <a href="#">DevTools</a>
        <ul class="dropdown-menu">
            <li class="dropdown-header">Dev Tools</li>
            <li><a href="javascript:{let e=(e,t=document)=>Array.from(t.querySelectorAll(e)),t=r=>{for(let t of e('link[rel=stylesheet][href]',r)){let e=new URL(t.href);e.searchParams.set('forceReload',Date.now()),t.href=e}for(let o of e('iframe',r))o.contentDocument&amp;&amp;t(o.contentDocument)};t()}" >Actualizar CSS</a></li>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/logs">Logs</a></li>
        </ul>
    </li>
</ul>