<div id="dashboard">

    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6">
                                <i class="fa fa-address-card-o fa-5x"></i>
                            </div>
                            <div class="col-xs-6 text-right">
                                <h3 class="announcement-heading"><?php echo $totalTerceros ?></h3>
                                <p class="announcement-text">Pacientes</p>
                            </div>
                        </div>
                    </div>

                        <div class="panel-footer announcement-bottom">
                            <div class="row">
                                <div class="col-xs-6">
                                    total pacientes
                                </div>
                            </div>
                        </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6">
                                <i class="fa fa-barcode fa-5x"></i>
                            </div>
                            <div class="col-xs-6 text-right">
                                <h3 class="announcement-heading"><?php echo $totalMuestras ?></h3>
                                <p class="announcement-text"> Muestras Activas</p>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo base_url(); ?>citotecnologo/muestra">
                        <div class="panel-footer announcement-bottom">
                            <div class="row">
                                <div class="col-xs-6">
                                    Ver Muestras
                                </div>
                                <div class="col-xs-6 text-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div><!-- /.row -->
    </div>

</div>