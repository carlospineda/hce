<?php
/**
 * [infMuestra] => stdClass Object
 * (
 * [primerNombre] => Paciente
 * [segundoNombre] =>
 * [primerApellido] => prueba
 * [segundoApellido] => Dos
 * [codTipoDocumento] => CC
 * [tipoDocumento] => Cedula
 * [numeroDocumento] => 123456789
 * [fechaNacimiento] => 1989-03-16
 * [fechaHora] => 2019-04-26 00:15:02
 * [estado] => A
 * [nombre] => Auxiliar
 * [apellido] => HCE
 * )
 *
 * [imgMuestra] => Array
 * (
 * [0] => stdClass Object
 * (
 * [idImagenMuestra] => 5
 * [idMuestra] => 4
 * [urlImagen] => e6131-i006-131.jpg
 * )
 *
 * [1] => stdClass Object
 * (
 * [idImagenMuestra] => 6
 * [idMuestra] => 4
 * [urlImagen] => 74ee6-i003-178.jpg
 * )
 *
 * [2] => stdClass Object
 * (
 * [idImagenMuestra] => 7
 * [idMuestra] => 4
 * [urlImagen] => b848f-i005-156.jpg
 * )
 *
 * )
 *
 * [antMuestra] => Array
 * (
 * [0] => stdClass Object
 * (
 * [idMuestraAntecedente] => 2
 * [idMuestra] => 4
 * [indActivoSexual] => 0
 * [fechaUltimaRelacionSexual] => 2019-05-08
 * [indGestacion] => 1
 * [indPartoVaginal] => 1
 * [indCesarea] => 1
 * [indAborto] => 1
 * [numeroHijosVivos] => 1
 * [fechaUltimaRegla] => 2019-05-24
 * [cicloMestrual] => R
 * [embarazada] => N
 * [indCitologiaPrevia] => 1
 * [fechaCitologiaPrevia] => 2019-05-15
 * [resultadoCitologiaPrevia] => N
 * [descripcionCitologiaPrevia] =>
 * [indPanifica] => 1
 * [metodoPlanificacion] => L
 * [procedimientosAnteriores] => 0
 * [aspectoCuello] => S
 * [observaciones] =>
 *
 * jnjkhkhjkhkj hkjh
 */
?>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Información de la muestra
            </div>
            <div class="panel-body clearfix"
                 style="position: relative;position: relative;padding: 1px 15px;background-color: #f9f9f9;">
                <i class="fa fa-user fa-4x" aria-hidden="true" style="float: left;margin: 14px;"></i>
                <h4 style="margin-bottom: 0px;">
                    <?php echo $infMuestra->primerNombre . ' ' . $infMuestra->segundoNombre . ' ' . $infMuestra->primerApellido . ' ' . $infMuestra->segundoApellido; ?>
                    <span class="badge"><? echo calcularAnios($infMuestra->fechaNacimiento) ?> años</span>
                </h4>
                <h5 style="margin: 1px;"><?php echo $infMuestra->codTipoDocumento . ' ' . $infMuestra->numeroDocumento ?></h5>
                <?php echo $infMuestra->fechaHora; ?><br>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Imagenes de la muestra
            </div>
            <div class="panel-body">
                <div style="text-align: center;">
                    <?php
                    if (isset($imgMuestra)) {
                        foreach ($imgMuestra as $img) {
                            echo '<a href="' . base_url() . 'assets/uploads/muestras/' . $img->urlImagen . '" class="img-muestra fancybox"><img src="' . base_url() . 'assets/uploads/muestras/' . $img->urlImagen . '" height="50px"></a>';
                        }
                    } else {
                        echo '<h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No se han encontrado imagenes anexas a la muetra</h4>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Formulario de respuesta
            </div>
            <div class="panel-body">

                <form class="form-horizontal" action="<?php echo base_url()?>citotecnologo/guardarDiagnostico" method="post">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>Información General de la Muestra</legend>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="slcTipoMuestra">Tipo de Muestra</label>
                            <div class="col-md-8">
                                <select id="slcTipoMuestra" name="slcTipoMuestra" class="form-control">
                                    <option value="a">Citología convencional</option>
                                    <option value="b">Citología de base liquida</option>
                                </select>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="slcCalidadMuestra">Calidad de la muestra</label>
                            <div class="col-md-8">
                                <select id="slcCalidadMuestra" name="slcCalidadMuestra" class="form-control">
                                    <option value="a">Adecuada para evaluacióńn  CON  presencia de endocervix</option>
                                    <option value="b">Adecuada para evaluacióńn  SIN presencia de endocervix</option>
                                    <option value="c">Muestra Rechazada</option>
                                    <option value="d">Muestra procesada y examinada, pero inadecuada</option>
                                </select>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="slcCategorizacionGeneral">Categorización general</label>
                            <div class="col-md-8">
                                <select id="slcCategorizacionGeneral" name="slcCategorizacionGeneral" class="form-control">
                                    <option value="a">Negativa para lesión intraepitelial o malignidad</option>
                                    <option value="b">Anomalías en células Epiteliales</option>
                                </select>
                            </div>
                        </div>

                        <div id="divAnomalia">

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcCategorizacionGeneralEspecifique">Especifique anomalia</label>
                                <div class="col-md-5">
                                    <select id="slcCategorizacionGeneralEspecifique" name="slcCategorizacionGeneralEspecifique" class="form-control">
                                        <option value="a">Escamoso</option>
                                        <option value="b">Glandular</option>
                                    </select>
                                </div>
                            </div>

                            <legend>Hallazgos No Neoplásicos</legend>

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcVariacionesNoNeo">Variaciones celulares no neoplásicas</label>
                                <div class="col-md-4">
                                    <select id="slcVariacionesNoNeo" name="slcVariacionesNoNeo" class="form-control">
                                        <option value="a">Metaplasia escamosa</option>
                                        <option value="b">Cambios queratósicos</option>
                                        <option value="c">Metaplasia tubaria</option>
                                        <option value="d">Atrofia</option>
                                        <option value="e">Cambios celulares asociados a embarazo</option>
                                        <option value="f">Otros</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcCambiosCelularesReactivos">Cambios celulares reactivos asociados a</label>
                                <div class="col-md-4">
                                    <select id="slcCambiosCelularesReactivos" name="slcCambiosCelularesReactivos" class="form-control">
                                        <option value="a">Inflamación (reparación típica)</option>
                                        <option value="b">Cervicitis linfocítica (folicular)</option>
                                        <option value="c">Radiación</option>
                                        <option value="d">Dispositivo intrauterino (DIU)</option>
                                        <option value="e">Células glandulares, estado post histerectomía</option>
                                        <option value="f">Otros</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcMicroorganismos">Microorganismos</label>
                                <div class="col-md-4">
                                    <select id="slcMicroorganismos" name="slcMicroorganismos" class="form-control">
                                        <option value="a">Trichomonas vaginalis</option>
                                        <option value="b">Levaduras/hifas de hongos compatible con Cándida spp.</option>
                                        <option value="c">Cambios en la flora sugestivos de vaginosis</option>
                                        <option value="d">Bacterias con morfologíáa compatible con Actinomyces spp.</option>
                                        <option value="e">Alteraciones celulares compatibles con virus herpes simplex</option>
                                        <option value="f">Alteraciones celulares compatibles con citomegalovirus.</option>
                                    </select>
                                </div>
                            </div>

                            <legend>Otros Hallazgos</legend>

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcPresenciaCelulasEndometriales">Presencia de células endometriales (en mujer de 45 años o más)</label>
                                <div class="col-md-4">
                                    <select id="slcPresenciaCelulasEndometriales" name="slcPresenciaCelulasEndometriales" class="form-control">
                                        <option value="a">Si</option>
                                        <option value="b">No</option>
                                    </select>
                                </div>
                            </div>

                            <legend>Anomalías de las Celulas Epiteliales</legend>

                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcCelulasEscamosas">Células escamosas</label>
                                <div class="col-md-4">
                                    <select id="slcCelulasEscamosas" name="slcCelulasEscamosas" class="form-control">
                                        <option value="a">Células escamosas atípicas</option>
                                        <option value="b">De significado no determinado (ASC-US)</option>
                                        <option value="c">No es posible excluir una lesión de alto grado (ASC-H)</option>
                                        <option value="d">Lesión escamosa intraepitelial de bajo grado (LEIBG). (Incluye VPH/ displasia leve/NIC 1)</option>
                                        <option value="e">Lesión escamosa intraepitelial de alto grado (LEIAG). (Incluye displasia moderada y grave, cáncer in situ; NIC 2 y NIC 3)</option>
                                        <option value="f">Carcinoma de células escamosas</option>
                                    </select>
                                </div>
                            </div>


                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="slcCelulasGlandulares">Células glandulares</label>
                                <div class="col-md-4">
                                    <select id="slcCelulasGlandulares" name="slcCelulasGlandulares" class="form-control">
                                        <option value="a">Atípicas</option>
                                        <option value="b">Células endocervicales (Sin otra especificación)</option>
                                        <option value="c">Células endometriales (Sin otra especificación)</option>
                                        <option value="d">Células glandulares (Sin otra especificación)</option>
                                        <option value="e">Atípicas</option>
                                        <option value="f">Células endocervicales probablemente neoplásicas</option>
                                        <option value="g">Células glandulares probablemente neoplásicas</option>
                                        <option value="h">Adenocarcinoma endocervical in situ</option>
                                        <option value="i">Adenocarcinoma</option>
                                        <option value="j">Endocervical</option>
                                        <option value="k">Endometria</option>
                                        <option value="l">Extrauterin</option>
                                        <option value="m">No especificado</option>
                                    </select>
                                </div>
                            </div>

                            <legend>Otras Neoplasias Malignas</legend>

                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="txtEspecificaciones">Especificaciones</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" id="txtEspecificaciones" name="txtEspecificaciones"></textarea>
                                </div>
                            </div>

                        </div>
                        <div>
                            <input type="submit" class="btn btn-primary btn-block" value="Guardar Diagnostico de la muestra">
                        </div>

                        <input type="hidden" id="txtIdMuestra" name="txtIdMuestra" value="<?php echo $idMuestra ?>" />
                    </fieldset>
                </form>


            </div>
        </div>


    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Antecedentes de la Muestra
            </div>
            <div class="panel-body">

                <?php
                if(isset($antMuestra)){
                ?>

                <dl class="dl-horizontal">
                    <dt>Activo Sexualmente?</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indActivoSexual) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <?php if ($antMuestra[0]->indActivoSexual) { ?>
                        <dt>Fecha Ultima Relación Sexual</dt>
                        <dd>
                            <?php echo $antMuestra[0]->fechaUltimaRelacionSexual ?>
                        </dd>
                    <? } ?>

                    <dt>Gestación?</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indGestacion) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <dt>Parto Vaginal?</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indPartoVaginal) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <dt>Cesarea?</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indCesarea) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <dt>Aborto?</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indAborto) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <dt>Numero de Hijos Vivos</dt>
                    <dd>
                        <?php echo $antMuestra[0]->numeroHijosVivos ?>
                    </dd>

                    <dt>Fecha de la Última Regla</dt>
                    <dd>
                        <?php echo $antMuestra[0]->fechaUltimaRegla ?>
                    </dd>

                    <dt>Ciclo Mestrual</dt>
                    <dd>
                        <?php
                        if ($antMuestra[0]->cicloMestrual == 'R') {
                            echo 'Regular';
                        } else if ($antMuestra[0]->cicloMestrual == 'I') {
                            echo 'Irregular';
                        }
                        ?>
                    </dd>

                    <dt>Embarazada</dt>
                    <dd>
                        <?php
                        if ($antMuestra[0]->embarazada == 'S') {
                            echo 'SI';
                        } else if ($antMuestra[0]->embarazada == 'N') {
                            echo 'NO';
                        } else if ($antMuestra[0]->embarazada == 'X') {
                            echo 'NO SABE';
                        }
                        ?>
                    </dd>

                    <dt>Citologia Previa</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indCitologiaPrevia) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <?php if ($antMuestra[0]->indCitologiaPrevia) { ?>
                        <dt>Resultado de la Citologia Previa</dt>
                        <dd>
                            <?php
                            if ($antMuestra[0]->resultadoCitologiaPrevia == 'N') {
                                echo 'NORMAL';
                            } else if ($antMuestra[0]->resultadoCitologiaPrevia == 'A') {
                                echo 'ANORMAL';
                            }
                            ?>
                        </dd>
                    <? } ?>

                    <dt>Actualmente Planifica</dt>
                    <dd>
                        <?php if ($antMuestra[0]->indPanifica) {
                            echo 'SI';
                        } else {
                            echo 'NO';
                        } ?>
                    </dd>

                    <?php if ($antMuestra[0]->indPanifica) { ?>
                        <dt>Que Metodo de Planificacción</dt>
                        <dd>
                            <?php
                            if ($antMuestra[0]->metodoPlanificacion == 'L') {
                                echo 'Ligadura';
                            } else if ($antMuestra[0]->metodoPlanificacion == 'P') {
                                echo 'Píldora';
                            } else if ($antMuestra[0]->metodoPlanificacion == 'R') {
                                echo 'Preservativo';
                            } else if ($antMuestra[0]->metodoPlanificacion == 'B') {
                                echo 'Barras';
                            } else if ($antMuestra[0]->metodoPlanificacion == 'D') {
                                echo 'DIU';
                            } else if ($antMuestra[0]->metodoPlanificacion == 'N') {
                                echo 'Natural';
                            } else if ($antMuestra[0]->metodoPlanificacion == 'I') {
                                echo 'Inyección';
                            }
                            ?>
                        </dd>
                    <? } ?>

                    <dt>Procedimientos anteriores en cuello uterino</dt>
                    <dd>
                        <?php
                        if ($antMuestra[0]->procedimientosAnteriores == 'N') {
                            echo 'Ninguno';
                        } else if ($antMuestra[0]->procedimientosAnteriores == 'C') {
                            echo 'Cauterizado';
                        } else if ($antMuestra[0]->procedimientosAnteriores == 'H') {
                            echo 'Histerectomía';
                        } else if ($antMuestra[0]->procedimientosAnteriores == 'O') {
                            echo 'Cotización';
                        } else if ($antMuestra[0]->procedimientosAnteriores == 'R') {
                            echo 'Radiación';
                        } else if ($antMuestra[0]->procedimientosAnteriores == 'T') {
                            echo 'Otro';
                        }
                        ?>
                    </dd>

                    <?php if ($antMuestra[0]->procedimientosAnteriores != 'N') { ?>
                        <dt>Aspecto del Cuello uterino</dt>
                        <dd>
                            <?php
                            if ($antMuestra[0]->aspectoCuello == 'A') {
                                echo 'Ausente';
                            } else if ($antMuestra[0]->aspectoCuello == 'S') {
                                echo 'Sano';
                            } else if ($antMuestra[0]->aspectoCuello == 'L') {
                                echo 'Lesión Visible';
                            } else if ($antMuestra[0]->aspectoCuello == 'T') {
                                echo 'Atrófico';
                            } else if ($antMuestra[0]->aspectoCuello == 'N') {
                                echo 'Sangrante';
                            } else if ($antMuestra[0]->aspectoCuello == 'Q') {
                                echo 'Quiste de Naboth';
                            } else if ($antMuestra[0]->aspectoCuello == 'P') {
                                echo 'Pólipo';
                            } else if ($antMuestra[0]->aspectoCuello == 'C') {
                                echo 'Congestivo';
                            } else if ($antMuestra[0]->aspectoCuello == 'U') {
                                echo 'Ulcerado';
                            } else if ($antMuestra[0]->aspectoCuello == 'E') {
                                echo 'Erosionado';
                            }
                            ?>
                        </dd>
                    <? } ?>


                </dl>
                <?php }else{
                    echo '<h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No se han encontrado información preliminar anexa a muestra!</h4>';
                } ?>
            </div>
        </div>

    </div>

</div>
<style>
    .img-muestra {
        margin-left: 6px;
    }

    @media (min-width: 768px) {
        .dl-horizontal dt {
            width: 300px;
        }

        .dl-horizontal dd {
            margin-left: 320px;
        }
    }

</style>