<div id="dashboard">

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo $total_pacientes ?></h3>

                    <p>Pacientes</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?php echo base_url(); ?>superadmin/pacientes" class="small-box-footer">ver más <i
                            class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo $total_ciudades ?></h3>

                    <p>Ciudades</p>
                </div>
                <div class="icon">
                    <i class="fa fa-list"></i>
                </div>
                <a href="<?php echo base_url(); ?>superadmin/ciudad" class="small-box-footer">ver más <i
                            class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo $total_departamentos ?></h3>

                    <p>Departamentos</p>
                </div>
                <div class="icon">
                    <i class="fa fa-th"></i>
                </div>
                <a href="<?php echo base_url(); ?>superadmin/departamento" class="small-box-footer">ver más <i
                            class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo $total_paises ?></h3>

                    <p>Paises</p>
                </div>
                <div class="icon">
                    <i class="fa fa-th-large"></i>
                </div>
                <a href="<?php echo base_url(); ?>superadmin/pais" class="small-box-footer">ver más <i
                            class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

</div>