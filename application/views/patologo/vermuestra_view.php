<?php
/**
 * [infMuestra] => stdClass Object
 * (
 * [primerNombre] => Paciente
 * [segundoNombre] =>
 * [primerApellido] => prueba
 * [segundoApellido] => Dos
 * [codTipoDocumento] => CC
 * [tipoDocumento] => Cedula
 * [numeroDocumento] => 123456789
 * [fechaNacimiento] => 1989-03-16
 * [fechaHora] => 2019-04-26 00:15:02
 * [estado] => A
 * [nombre] => Auxiliar
 * [apellido] => HCE
 * )
 *
 * [imgMuestra] => Array
 * (
 * [0] => stdClass Object
 * (
 * [idImagenMuestra] => 5
 * [idMuestra] => 4
 * [urlImagen] => e6131-i006-131.jpg
 * )
 *
 * [1] => stdClass Object
 * (
 * [idImagenMuestra] => 6
 * [idMuestra] => 4
 * [urlImagen] => 74ee6-i003-178.jpg
 * )
 *
 * [2] => stdClass Object
 * (
 * [idImagenMuestra] => 7
 * [idMuestra] => 4
 * [urlImagen] => b848f-i005-156.jpg
 * )
 *
 * )
 *
 * [antMuestra] => Array
 * (
 * [0] => stdClass Object
 * (
 * [idMuestraAntecedente] => 2
 * [idMuestra] => 4
 * [indActivoSexual] => 0
 * [fechaUltimaRelacionSexual] => 2019-05-08
 * [indGestacion] => 1
 * [indPartoVaginal] => 1
 * [indCesarea] => 1
 * [indAborto] => 1
 * [numeroHijosVivos] => 1
 * [fechaUltimaRegla] => 2019-05-24
 * [cicloMestrual] => R
 * [embarazada] => N
 * [indCitologiaPrevia] => 1
 * [fechaCitologiaPrevia] => 2019-05-15
 * [resultadoCitologiaPrevia] => N
 * [descripcionCitologiaPrevia] =>
 * [indPanifica] => 1
 * [metodoPlanificacion] => L
 * [procedimientosAnteriores] => 0
 * [aspectoCuello] => S
 * [observaciones] =>
 *
 * jnjkhkhjkhkj hkjh
 */
?>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Información de la muestra
            </div>
            <div class="panel-body clearfix"
                 style="position: relative;position: relative;padding: 1px 15px;background-color: #f9f9f9;">
                <i class="fa fa-user fa-4x" aria-hidden="true" style="float: left;margin: 14px;"></i>
                <h4 style="margin-bottom: 0px;">
                    <?php echo $infMuestra->primerNombre . ' ' . $infMuestra->segundoNombre . ' ' . $infMuestra->primerApellido . ' ' . $infMuestra->segundoApellido; ?>
                    <span class="badge"><? echo calcularAnios($infMuestra->fechaNacimiento) ?> años</span>
                </h4>
                <h5 style="margin: 1px;"><?php echo $infMuestra->codTipoDocumento . ' ' . $infMuestra->numeroDocumento ?></h5>
                <?php echo $infMuestra->fechaHora; ?><br>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Imagenes de la muestra
            </div>
            <div class="panel-body">
                <div style="text-align: center;">
                    <?php
                    if (isset($imgMuestra)) {
                        foreach ($imgMuestra as $img) {
                            echo '<a href="' . base_url() . 'assets/uploads/muestras/' . $img->urlImagen . '" class="img-muestra fancybox"><img src="' . base_url() . 'assets/uploads/muestras/' . $img->urlImagen . '" height="50px"></a>';
                        }
                    } else {
                        echo '<h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No se han encontrado imagenes anexas a la muetra</h4>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
/**
 * idMuestraRespuesta] => 7
 * idMuestra] => 7
 * idUsuario] => 13
 *
 * tipoMuestra] => a
 * ] => a
 * ] => b
 * ] => a
 * ] => a
 * ] => a
 * ] => a
 * ] => a
 * ] => a
 * ] => a
 * ] =>
 * fechaHora] => 2019-05-29 11:54:02
 * indConfirmacion] =>
 * textoConfirmacion] =>
 * fechaHoraConfirmacion]
 *
 *
 * test($diaMuestra, true);
 */
?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Confirmar Diagnostico
            </div>
            <div class="panel-body">

                <form class="form-horizontal" action="<?php echo base_url()?>patologo/guardarDiagnostico" method="post">
                    <fieldset>
                        <!-- Multiple Radios -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="rdbConfirmarDiagnostico">Confirmar
                                Diagnostico</label>
                            <div class="col-md-4">
                                <div class="radio">
                                    <label for="rdbConfirmarDiagnostico-0">
                                        <input type="radio" name="rdbConfirmarDiagnostico"
                                               id="rdbConfirmarDiagnostico-0" value="1" checked="checked">
                                        Si
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="rdbConfirmarDiagnostico-1">
                                        <input type="radio" name="rdbConfirmarDiagnostico"
                                               id="rdbConfirmarDiagnostico-1" value="0">
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="txtConfirmarDiagnostico">Descripción</label>
                            <div class="col-md-8">
                                <textarea class="form-control" id="txtConfirmarDiagnostico"
                                          name="txtConfirmarDiagnostico"></textarea>
                            </div>
                        </div>


                    </fieldset>
                    <input type="hidden" id="idMuestraRespuesta" name="idMuestraRespuesta" value="<?php echo $diaMuestra[0]->idMuestraRespuesta ?>" />
                    <input type="hidden" id="txtIdMuestra" name="txtIdMuestra" value="<?php echo $idMuestra ?>" />

                    <input type="submit" value="Enviar Confirmación" class="btn btn-block btn-primary" />

                </form>

            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                Diagnostico
            </div>
            <div class="panel-body">


                <dl class="dl-horizontal">
                    <dt>Tipo de Muestra</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->tipoMuestra == 'a') {
                            echo 'Citología convencional';
                        } else if ($diaMuestra[0]->tipoMuestra == 'b') {
                            echo 'Citología de base liquida';
                        } ?>
                    </dd>

                    <dt>Calidad de la muestra</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->calidadMuestra == 'a') {
                            echo 'Adecuada para evaluacióńn CON presencia de endocervix';
                        } else if ($diaMuestra[0]->calidadMuestra == 'b') {
                            echo 'Adecuada para evaluacióńn SIN presencia de endocervix';
                        } else if ($diaMuestra[0]->calidadMuestra == 'c') {
                            echo 'Muestra Rechazada';
                        } else if ($diaMuestra[0]->calidadMuestra == 'd') {
                            echo 'Muestra procesada y examinada, pero inadecuada';
                        } ?>
                    </dd>

                    <dt>Categorización general</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->categorizacionGeneral == 'a') {
                            echo 'Negativa para lesión intraepitelial o malignidad';
                        } else if ($diaMuestra[0]->categorizacionGeneral == 'b') {
                            echo 'Anomalías en células Epiteliales';
                        } ?>
                    </dd>

                    <dt>Especifique anomalia</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->categorizacionGeneralEspecifique == 'a') {
                            echo 'Escamoso';
                        } else if ($diaMuestra[0]->categorizacionGeneralEspecifique == 'b') {
                            echo 'Glandular';
                        } ?>
                    </dd>

                    <dt>Variaciones celulares no neoplásicas</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->variacionesNoNeo == 'a') {
                            echo 'Metaplasia escamosa';
                        } else if ($diaMuestra[0]->variacionesNoNeo == 'b') {
                            echo 'Cambios queratósicos';
                        } else if ($diaMuestra[0]->variacionesNoNeo == 'c') {
                            echo 'Metaplasia tubaria';
                        } else if ($diaMuestra[0]->variacionesNoNeo == 'd') {
                            echo 'Atrofia';
                        } else if ($diaMuestra[0]->variacionesNoNeo == 'e') {
                            echo 'Cambios celulares asociados a embarazo';
                        } else if ($diaMuestra[0]->variacionesNoNeo == 'f') {
                            echo 'Otros';
                        } ?>
                    </dd>

                    <dt>Cambios celulares reactivos asociados a</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->cambiosCelularesReactivos == 'a') {
                            echo 'Inflamación (reparación típica)';
                        } else if ($diaMuestra[0]->cambiosCelularesReactivos == 'b') {
                            echo 'Cervicitis linfocítica (folicular)';
                        } else if ($diaMuestra[0]->cambiosCelularesReactivos == 'c') {
                            echo 'Radiación';
                        } else if ($diaMuestra[0]->cambiosCelularesReactivos == 'd') {
                            echo 'Dispositivo intrauterino (DIU)';
                        } else if ($diaMuestra[0]->cambiosCelularesReactivos == 'e') {
                            echo 'Células glandulares, estado post histerectomía';
                        } else if ($diaMuestra[0]->cambiosCelularesReactivos == 'f') {
                            echo 'Otros';
                        } ?>
                    </dd>

                    <dt>Microorganismos</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->microorganismos == 'a') {
                            echo 'Trichomonas vaginalis';
                        } else if ($diaMuestra[0]->microorganismos == 'b') {
                            echo 'Levaduras/hifas de hongos compatible con Cándida spp.';
                        } else if ($diaMuestra[0]->microorganismos == 'c') {
                            echo 'Cambios en la flora sugestivos de vaginosis';
                        } else if ($diaMuestra[0]->microorganismos == 'd') {
                            echo 'Bacterias con morfologíáa compatible con Actinomyces spp.';
                        } else if ($diaMuestra[0]->microorganismos == 'e') {
                            echo 'Alteraciones celulares compatibles con virus herpes simplex';
                        } else if ($diaMuestra[0]->microorganismos == 'f') {
                            echo 'Alteraciones celulares compatibles con citomegalovirus.';
                        } ?>
                    </dd>

                    <dt>Presencia de células endometriales (en mujer de 45 años o más)</dt>
                    <dd>

                        <?php if ($diaMuestra[0]->presenciaCelulasEndometriales == 'a') {
                            echo 'Si';
                        } else if ($diaMuestra[0]->presenciaCelulasEndometriales == 'b') {
                            echo 'No';
                        } ?>

                    </dd>

                    <dt>Células escamosas</dt>
                    <dd>
                        <?php if ($diaMuestra[0]->celulasEscamosas == 'a') {
                            echo 'Células escamosas atípicas';
                        } else if ($diaMuestra[0]->celulasEscamosas == 'b') {
                            echo 'De significado no determinado (ASC-US)';
                        } else if ($diaMuestra[0]->celulasEscamosas == 'c') {
                            echo 'No es posible excluir una lesión de alto grado (ASC-H)';
                        } else if ($diaMuestra[0]->celulasEscamosas == 'd') {
                            echo 'Lesión escamosa intraepitelial de bajo grado (LEIBG). (Incluye
                            VPH/ displasia leve/NIC 1)';
                        } else if ($diaMuestra[0]->celulasEscamosas == 'e') {
                            echo 'Lesión escamosa intraepitelial de alto grado (LEIAG). (Incluye
                            displasia moderada y grave, cáncer in situ; NIC 2 y NIC 3)';
                        } else if ($diaMuestra[0]->celulasEscamosas == 'f') {
                            echo 'Carcinoma de células escamosas';
                        } ?>
                    </dd>

                    <dt>Células glandulares</dt>
                    <dd>

                        <?php if ($diaMuestra[0]->celulasGlandulares == 'a') {
                            echo 'Atípicas';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'b') {
                            echo 'Células endocervicales (Sin otra especificación)';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'c') {
                            echo 'Células endometriales (Sin otra especificación)';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'd') {
                            echo 'Células glandulares (Sin otra especificación)';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'e') {
                            echo 'Atípicas';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'f') {
                            echo 'Células endocervicales probablemente neoplásicas';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'g') {
                            echo 'Células glandulares probablemente neoplásicas';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'h') {
                            echo 'Adenocarcinoma endocervical in situ';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'i') {
                            echo 'Adenocarcinoma';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'j') {
                            echo 'Endocervical';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'k') {
                            echo 'Endometria';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'l') {
                            echo 'Extrauterin';
                        } else if ($diaMuestra[0]->celulasGlandulares == 'm') {
                            echo 'No especificado';
                        } ?>
                    </dd>

                    <dt>Especificaciones</dt>
                    <dd>
                        <?php echo $diaMuestra[0]->especificaciones ?>
                    </dd>

                </dl>

            </div>
        </div>


    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Antecedentes de la Muestra
            </div>
            <div class="panel-body">

                <?php
                if (isset($antMuestra)) {
                    ?>

                    <dl class="dl-horizontal">
                        <dt>Activo Sexualmente?</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indActivoSexual) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <?php if ($antMuestra[0]->indActivoSexual) { ?>
                            <dt>Fecha Ultima Relación Sexual</dt>
                            <dd>
                                <?php echo $antMuestra[0]->fechaUltimaRelacionSexual ?>
                            </dd>
                        <? } ?>

                        <dt>Gestación?</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indGestacion) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <dt>Parto Vaginal?</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indPartoVaginal) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <dt>Cesarea?</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indCesarea) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <dt>Aborto?</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indAborto) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <dt>Numero de Hijos Vivos</dt>
                        <dd>
                            <?php echo $antMuestra[0]->numeroHijosVivos ?>
                        </dd>

                        <dt>Fecha de la Última Regla</dt>
                        <dd>
                            <?php echo $antMuestra[0]->fechaUltimaRegla ?>
                        </dd>

                        <dt>Ciclo Mestrual</dt>
                        <dd>
                            <?php
                            if ($antMuestra[0]->cicloMestrual == 'R') {
                                echo 'Regular';
                            } else if ($antMuestra[0]->cicloMestrual == 'I') {
                                echo 'Irregular';
                            }
                            ?>
                        </dd>

                        <dt>Embarazada</dt>
                        <dd>
                            <?php
                            if ($antMuestra[0]->embarazada == 'S') {
                                echo 'SI';
                            } else if ($antMuestra[0]->embarazada == 'N') {
                                echo 'NO';
                            } else if ($antMuestra[0]->embarazada == 'X') {
                                echo 'NO SABE';
                            }
                            ?>
                        </dd>

                        <dt>Citologia Previa</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indCitologiaPrevia) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <?php if ($antMuestra[0]->indCitologiaPrevia) { ?>
                            <dt>Resultado de la Citologia Previa</dt>
                            <dd>
                                <?php
                                if ($antMuestra[0]->resultadoCitologiaPrevia == 'N') {
                                    echo 'NORMAL';
                                } else if ($antMuestra[0]->resultadoCitologiaPrevia == 'A') {
                                    echo 'ANORMAL';
                                }
                                ?>
                            </dd>
                        <? } ?>

                        <dt>Actualmente Planifica</dt>
                        <dd>
                            <?php if ($antMuestra[0]->indPanifica) {
                                echo 'SI';
                            } else {
                                echo 'NO';
                            } ?>
                        </dd>

                        <?php if ($antMuestra[0]->indPanifica) { ?>
                            <dt>Que Metodo de Planificacción</dt>
                            <dd>
                                <?php
                                if ($antMuestra[0]->metodoPlanificacion == 'L') {
                                    echo 'Ligadura';
                                } else if ($antMuestra[0]->metodoPlanificacion == 'P') {
                                    echo 'Píldora';
                                } else if ($antMuestra[0]->metodoPlanificacion == 'R') {
                                    echo 'Preservativo';
                                } else if ($antMuestra[0]->metodoPlanificacion == 'B') {
                                    echo 'Barras';
                                } else if ($antMuestra[0]->metodoPlanificacion == 'D') {
                                    echo 'DIU';
                                } else if ($antMuestra[0]->metodoPlanificacion == 'N') {
                                    echo 'Natural';
                                } else if ($antMuestra[0]->metodoPlanificacion == 'I') {
                                    echo 'Inyección';
                                }
                                ?>
                            </dd>
                        <? } ?>

                        <dt>Procedimientos anteriores en cuello uterino</dt>
                        <dd>
                            <?php
                            if ($antMuestra[0]->procedimientosAnteriores == 'N') {
                                echo 'Ninguno';
                            } else if ($antMuestra[0]->procedimientosAnteriores == 'C') {
                                echo 'Cauterizado';
                            } else if ($antMuestra[0]->procedimientosAnteriores == 'H') {
                                echo 'Histerectomía';
                            } else if ($antMuestra[0]->procedimientosAnteriores == 'O') {
                                echo 'Cotización';
                            } else if ($antMuestra[0]->procedimientosAnteriores == 'R') {
                                echo 'Radiación';
                            } else if ($antMuestra[0]->procedimientosAnteriores == 'T') {
                                echo 'Otro';
                            }
                            ?>
                        </dd>

                        <?php if ($antMuestra[0]->procedimientosAnteriores != 'N') { ?>
                            <dt>Aspecto del Cuello uterino</dt>
                            <dd>
                                <?php
                                if ($antMuestra[0]->aspectoCuello == 'A') {
                                    echo 'Ausente';
                                } else if ($antMuestra[0]->aspectoCuello == 'S') {
                                    echo 'Sano';
                                } else if ($antMuestra[0]->aspectoCuello == 'L') {
                                    echo 'Lesión Visible';
                                } else if ($antMuestra[0]->aspectoCuello == 'T') {
                                    echo 'Atrófico';
                                } else if ($antMuestra[0]->aspectoCuello == 'N') {
                                    echo 'Sangrante';
                                } else if ($antMuestra[0]->aspectoCuello == 'Q') {
                                    echo 'Quiste de Naboth';
                                } else if ($antMuestra[0]->aspectoCuello == 'P') {
                                    echo 'Pólipo';
                                } else if ($antMuestra[0]->aspectoCuello == 'C') {
                                    echo 'Congestivo';
                                } else if ($antMuestra[0]->aspectoCuello == 'U') {
                                    echo 'Ulcerado';
                                } else if ($antMuestra[0]->aspectoCuello == 'E') {
                                    echo 'Erosionado';
                                }
                                ?>
                            </dd>
                        <? } ?>


                    </dl>
                <?php } else {
                    echo '<h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No se han encontrado información preliminar anexa a muestra!</h4>';
                } ?>
            </div>
        </div>

    </div>

</div>
<style>
    .img-muestra {
        margin-left: 6px;
    }

    @media (min-width: 768px) {
        .dl-horizontal dt {
            width: 300px;
        }

        .dl-horizontal dd {
            margin-left: 320px;
        }
    }

</style>