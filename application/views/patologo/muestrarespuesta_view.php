<form class="form-horizontal" action="../guardarrespuesta" method="post">
    <fieldset>

        <!-- Form Name -->
        <legend>Agregar Respuesta a la muestra</legend>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-2 control-label" for="txt_respuesta">Respuesta</label>
            <div class="col-md-8">
                <textarea class="form-control" id="txt_respuesta" name="txt_respuesta" style="height: 200px;"></textarea>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <div class="col-md-12">
                <input type="submit" id="singlebutton" name="singlebutton"
                       class="btn btn-primary btn-block" value="Guardar Respuesta">
                </input>
            </div>
        </div>

    </fieldset>
    <input name="idmuestra" type="hidden" value="<?php echo $idmuestra ?>">
</form>