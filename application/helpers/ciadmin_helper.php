<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Helper base para Codeigniter CIAdmin
 * Versión : 1.3.3
 * Autor : Ciberdix - CarlosPinedat
 *
 * https://gist.github.com/CarlosPinedaT/ffd3ec15a0a892824b9a87d4b4c2546e
 */
if (!function_exists('ca_growl')) {
    /**
     * Función que guarda datos en sesion temporal para mostrar Growls en la vista
     *
     * @param string $titulo Titulo del mensaje
     * @param string $mensaje Descripcion del mensaje
     * @param int $tipo Numero tipo 1:Info 2:Success 3:Warning 4:Danger
     *
     * @return Bolean True
     */
    function ca_growl($titulo, $mensaje, $tipo = 1)
    {
        $CI = &get_instance();
        switch ($tipo) {
            case 1:
                $tipo = 'info';
                $icono = 'fa-info-circle';
                break;
            case 2:
                $tipo = 'success';
                $icono = 'fa-check-circle';
                break;
            case 3:
                $tipo = 'warning';
                $icono = 'fa-exclamation-triangle';
                break;
            case 4:
                $tipo = 'danger';
                $icono = 'fa-times-circle';
                break;
            default:
                $tipo = 'info';
                $icono = 'fa-info-circle';
        }
        $CI->session->set_flashdata('msgTitulo', $titulo);
        $CI->session->set_flashdata('msgMensaje', $mensaje);
        $CI->session->set_flashdata('msgTipo', $tipo);
        $CI->session->set_flashdata('msgIcono', $icono);
        return true;
    }
}
if (!function_exists("addBreadLink")) {
    /**
     * Agrega Miga de Pan
     *
     * @param type $link
     * @param type $label
     * @return string
     */
    function addBreadLink($label = "", $link = "")
    {
        if ($link != '')
            return '<li> <a href="' . site_url($link) . '">' . $label . '</a></li>';
        else
            return '<li>' . $label . '</li>';
    }
}
if (!function_exists("money")) {
    /**
     * Formato de moneda
     *
     * @param String $money
     * @return String
     */
    function money($money)
    {
        return number_format($money, 0, ',', '.');
    }
}
if (!function_exists("url_encode")) {
    /**
     * Limpia string para usar como una URL
     *
     * @param String $url
     * @return String
     */
    function url_encode($url)
    {
        $title = strtolower($url);
        $polskie = array(',', ' - ', ' ', 'ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń', '-', "'", "/", "?", '"', ":", 'ś', '!', '.', '&', '&amp;', '#', ';', '[', ']', 'domena.pl', '(', ')', '`', '%', '”', '„', '…');
        $miedzyn = array('-', '-', '-', 'e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n', '-', "", "", "", "", "", 's', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        $string = str_replace($polskie, $miedzyn, $title);
        // usuń wszytko co jest niedozwolonym znakiem
        $string = preg_replace('/[^0-9a-z\-]+/', '', $string);
        // zredukuj liczbę myślników do jednego obok siebie
        $string = preg_replace('/[\-]+/', '-', $string);
        // usuwamy możliwe myślniki na początku i końcu
        $string = trim($string, '-');
        $string = stripslashes($string);
        // na wszelki wypadek
        $string = urlencode($string);
        return $string;
    }
}
if (!function_exists('jsredirect')) {
    /**
     * Acción atravez del cual se hace un redirect con javascript a una url determinada
     *
     * @param type $url
     * @param type $top
     */
    function jsredirect($url, $top = false)
    {
        $script_redirect = '<script type="text/javascript">';
        $script_redirect .= 'window';
        if ($top)
            $script_redirect .= '.top';
        $script_redirect .= '.location.href="' . $url . '"';
        $script_redirect .= '</script>';
        echo $script_redirect;
        die();
    }
}
if (!function_exists('test')) {
    /**
     * Generar test de variables
     *
     * @param type $x
     * @param type $exit
     */
    function test($x, $exit = false)
    {
        echo $res = "<pre>";
        if (is_array($x) || is_object($x)) {
            echo print_r($x);
        } else {
            echo var_dump($x);
        }
        echo "</pre><hr />";
        if ($exit) {
            die();
        }
    }
}
if (!function_exists('send_email')) {
    require(FCPATH . 'assets/email/sendgrid/sendgrid-php.php');
    /**
     * Enviar un Email a travez de un servidor de correo establecido
     *
     * @param type $para
     * @param type $de
     * @param type $deNombre
     * @param type $titulo
     * @param type $mensaje
     * @param type $template
     * @return type boolean
     */
    function send_email($para, $de, $deNombre, $titulo, $mensaje, $template = 'index')
    {
        $urlEmailFolder = FCPATH . 'assets/email/template/';
        $urlEmail = $urlEmailFolder . '' . $template . '.html';
        $message = file_get_contents($urlEmail);
        $message = str_replace('%titulo%', $titulo, $message);
        $message = str_replace('%contenido%', $mensaje, $message);
        // Login SendGrid >> https://sendgrid.com/
        $sendgrid = new SendGrid('mascotapps', 'qwerty1234.00');
        $email = new SendGrid\Email();
        $email->addTo($para)
            ->setFrom($de)
            ->setFromName($deNombre)
            ->setSubject($titulo)
            ->setHtml($message);
        try {
            $rta = $sendgrid->send($email);
        } catch (\SendGrid\Exception $e) {
            $rta = $e->getCode();
            foreach ($e->getErrors() as $er) {
                $rta .= $er;
            }
        }
        return $rta;
    }
}
if (!function_exists('ahora')) {
    /**
     * Establecer Fecha Hora de colombia
     *
     * @return type String
     */
    function ahora()
    {
        date_default_timezone_set('America/Bogota');
        return date('Y-m-d H:i:s');
    }
}
if (!function_exists('returnJson')) {
    /**
     * Establce el retorno en JSON
     *
     * @param type $array
     */
    function returnJson($array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
    }
}
if (!function_exists('calcularAnios')) {
    /**
     * Calcula edad con base en fecha de nacimiento
     * @param $fechaNacimiento
     * @return int
     */
    function calcularAnios($fechaNacimiento)
    {
        $cumpleanos = new DateTime($fechaNacimiento);
        $hoy = new DateTime();
        $annos = $hoy->diff($cumpleanos);
        return $annos->y;
    }
}
if (!function_exists('generarUuid')) {

    /**
     * Genera un UUID v4
     * @return string
     */
    function generarUuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}